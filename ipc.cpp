#include "ipc.hpp"
#include <unistd.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

IPC::IPC(const char *name, bool host, bool _debug) : debug(_debug) {
    char othernamebuf[255];
    char otherfullpath[512];
    int totalslept = 0;
    is_host = host;

    tot_send = tot_recv = 0;

    assert(strlen(name) < 250);
    strcpy(namebuf, name);
    namebuf[strlen(name)] = '.';
    if (host)
        namebuf[strlen(name)+1] = 'H';
    else
        namebuf[strlen(name)+1] = 'R';
    namebuf[strlen(name)+2] = '\0';
    strcpy(othernamebuf, namebuf);
    if (host)
        othernamebuf[strlen(name)+1] = 'R';
    else
        othernamebuf[strlen(name)+1] = 'H';

    message_queue::remove(namebuf);
#ifdef DEBUG
    printf("Attempting to open send message queue: %s\n", namebuf);
#endif

    mq_send = new message_queue(create_only, namebuf, 1000, sizeof(tmp));

    while (1) {
        usleep(1000);
        totalslept++;
        try {
            mq_recv = new message_queue(open_only, othernamebuf);
            break;
        } catch (...) {
            printf("Sleeping for other message queue.\n");
        }
        asserte(totalslept < 1000, "timing out for other message queue");
    }
}

void IPC::asserte(bool cond, const char *err) {
    if (cond)
        return;
    perror(err);
    fprintf(stderr, "Fatal error in PID %d\n", getpid());
    this->~IPC();
    assert(false);
}

IPC::~IPC() {
#ifdef DEBUG
    printf("Freeing message queue: %s\n", namebuf);
#endif
    printf("IPC layer sent %lld msgs recvd %lld msgs\n", tot_send, tot_recv);
    delete mq_send;
    //assert(!mq_unlink(namebuf));
}

void IPC::write_count(uint32_t count, uint32_t succ_type) {
    memset(&tmp, 0, sizeof(tmp));
    tmp.count.type = k_type_count;
    tmp.count.count = count;
    tmp.count.succ_type = succ_type;
#ifdef DEBUG
    print_msg_in_buf();
#endif
    tot_send++;
    mq_send->send(&tmp, sizeof(tmp), 0);
    //assert(!mq_send(queue_send, (char*) &tmp, sizeof(tmp), 0));
}

void IPC::write_ready(uint32_t node, uint32_t cl, uint32_t ready_for) {
    tmp.ready.type = k_type_ready;
    tmp.ready.node = node;
    tmp.ready.cl = cl;
    tmp.ready.ready_for = ready_for;
#ifdef DEBUG
    print_msg_in_buf();
#endif
    tot_send++;
    mq_send->send(&tmp, sizeof(tmp), 0);
    //assert(!mq_send(queue_send, (char*) &tmp, sizeof(tmp), 0));
}

void IPC::write_message(uint32_t src, uint32_t dst, uint32_t cl, void *data) {
    tmp.message.type = k_type_message;
    tmp.message.src = src;
    tmp.message.dst = dst;
    tmp.message.cl = cl;
    tmp.message.data = data;
#ifdef DEBUG
    print_msg_in_buf();
#endif
    tot_send++;
    mq_send->send(&tmp, sizeof(tmp), 0);
    //assert(!mq_send(queue_send, (char*) &tmp, sizeof(tmp), 0));
}

void IPC::write_command(uint32_t op) {
    tmp.ctrl.type = k_type_simctrl;
    tmp.ctrl.op = op;
#ifdef DEBUG
    print_msg_in_buf();
#endif
    tot_send++;
    mq_send->send(&tmp, sizeof(tmp), 0);
    //assert(!mq_send(queue_send, (char*) &tmp, sizeof(tmp), 0));
}

union ipc_msg *IPC::read_message() {
    int ret;
    size_t discard_size;
    unsigned int discard_int;
    tot_recv++;
    mq_recv->receive(&tmp, sizeof(tmp), discard_size, discard_int);
    //ret = mq_receive(queue_recv, (char*) &tmp, sizeof(tmp), NULL);
    //asserte(ret != -1, "could not read message");
#ifdef DEBUG
    print_msg_in_buf(false);
#endif
    return &tmp;
}

void IPC::print_msg_in_buf(bool send) {
    printf("%c(%c): ", is_host ? 'H' : 'R', send ? 'S' : 'R');
    switch(tmp.count.type) {
        case k_type_count:
            printf("Count: %d of type %d\n", tmp.count.count, tmp.count.succ_type);
            break;
        case k_type_ready:
            printf("Ready: %d for %d messages\n", tmp.ready.node, tmp.ready.ready_for);
            break;
        case k_type_message:
            printf("Msg: %d-%d>%d (%p)\n", tmp.message.src, tmp.message.cl, tmp.message.dst, tmp.message.data);
            break;
        case k_type_simctrl:
            printf("Ctrl: %d\n", tmp.ctrl.op);
            break;
        default:
            printf("Unknown: %d/%d/%d/%p\n", tmp.message.type,
                                             tmp.message.src,
                                             tmp.message.dst,
                                             tmp.message.data);
            assert(false);
            break;
    }
}

