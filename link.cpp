#include "link.h"

void
CreditLink::set_lat(int _lat, int from, int to) {
    int i, j, k;

    _lat += 1;
    printf("Set lat %d from %d to %d\n", _lat, from, to);
    if (from != 0) {
      fprintf(stderr, "CreditLink can only have one sender.\n");
      assert(0);
    }

    if (lat.size() <= to) {
        lat.resize(to+1);
        credits.resize(to+1, 0);
    }

    lat[to] = _lat;

    pipelines_data.resize(lat.size());
    for (i=0; i<lat.size(); i++) {
      pipelines_data[i].clear();
      for (k=0; k<lat[i]; k++) {
        pipelines_data[i].push_back(false);
      }
    }

    pipelines_credits.resize(lat.size());
    for (i=0; i<lat.size(); i++) {
      pipelines_credits[i].clear();
      for (k=0; k<lat[i]; k++) {
        pipelines_credits[i].push_back(0);
      }
    }
}


bool
CreditLink::can_send(Node *sender) {
  bool starve = false;
  for (int c: credits) 
    starve |= !c;
  return !starve;
}

bool
CreditLink::send(Node *sender) {
  if (!can_send(sender))
    return false;

  for (int& c: credits) 
    c--;

  for (int i=0; i<pipelines_data.size(); i++) {
    assert(!pipelines_data[i].back());
    pipelines_data[i].back() = true;
  }

  return true;
}

void
CreditLink::clock(uint64_t cycle) {
  /* Start by advancing all links. */
    for (int i=0; i<pipelines_data.size(); i++) {
        assert(!pipelines_data[i].front());
        pipelines_data[i].pop_front();
        pipelines_data[i].push_back(false);
    }
    /* Process credits. */
    for (int to=0; to<pipelines_credits.size(); to++) {
      credits[to] += pipelines_credits[to].front();
    }
    for (int i=0; i<pipelines_credits.size(); i++) {
        pipelines_credits[i].pop_front();
        pipelines_credits[i].push_back(0);
    }
}

void
CreditLink::credit(Node *receiver) {
  int recv_id = dst_node[receiver];
  pipelines_credits[recv_id].back()++;
  //printf("Link %s receive credit.\n", name.c_str());
}

int
CreditLink::try_collect_token(Node *receiver) {
  int recv_id = dst_node[receiver];
  if(!pipelines_data[recv_id].front())
    return 0;
  pipelines_data[recv_id].front() = false;
  return 1;
}

void
CreditLink::set_param(string param, string val, vector<int> &index) {
    if (param == "lat") {
        if (index.size() == 2)
            set_lat(atoi(val.c_str()), index[0], index[1]);
        else if (!index.size()) 
            set_lat(atoi(val.c_str()), 0, 0);
        else
            assert(false);
    } else {
        Link::set_param(param, val, index);
    }
}

void
Link::set_param(string param, string val, vector<int> &index) {
    if (param == "type") {
        if (val == "vec") {
          type = k_vec;
          bits = 512;
        } else if (val == "scal") {
          type = k_scal;
          bits = 32;
        } else if (val == "ctrl") {
          type = k_ctrl;
          bits = 1;
        } else {
          assert(false);
        }
    } else if (param == "count") {
      expected_count = atoi(val.c_str());
    } else if (param == "trace") {
        if (val == "on" || val == "1") trace = true;
        else if (val == "off" || val == "0") trace = false;
        else assert(false);
    } else if (param == "prog_id") {
        assert(index.size() == 0);
        prog_id = atoi(val.c_str());
    } else {
        //fprintf(stderr, "Error setting %s[%d] = %s\n", param.c_str(), index[0], val.c_str());
        fprintf(stderr, "Error setting %s = %s\n", param.c_str(), val.c_str());
        throw std::invalid_argument(param +": " + val);
    }
}

void BufferedStaticLink::set_param(string param, string val, vector<int> &index) {
    if (param == "lat") {
        if (index.size() == 2)
            set_lat(atoi(val.c_str()), index[0], index[1]);
        else if (!index.size()) 
            set_lat(atoi(val.c_str()), 0, 0);
        else
            assert(false);
    } else {
        Link::set_param(param, val, index);
    }
}

void BufferedStaticLink::clock(uint64_t cycle) {
    int i, j, k;
    bool stall = false;
    for (i=0; i<pipelines.size(); i++) {
        for (j=0; j<pipelines[i].size(); j++) {
          vector<bool> gen_stall(pipelines[i][j].size());
          for (k=0; k<pipelines[i][j].size(); k++) {
            gen_stall[k] = (pipelines[i][j][k] == buf_depth);
            assert(pipelines[i][j][k] <= buf_depth);
          }
          for (k=pipelines[i][j].size()-2; k>=0; k--) {
            /* Don't allow propagation if we had a previous stall. */
            if (gen_stall[k])
              continue;
            /* Can't propagate a token if none exists in the previous stage. */
            if (!pipelines[i][j][k+1])
              continue;
            assert(++pipelines[i][j][k] <= buf_depth);
            pipelines[i][j][k+1]--;
          }
        }
    }
}

bool BufferedStaticLink::can_send(Node *sender) {
    int i;
    int send_id = src_node[sender];
    bool stall = false;
    for (i=0; i<pipelines.size(); i++)
        stall |= (pipelines[i][send_id].back() == buf_depth);

    return !stall;
}

bool BufferedStaticLink::send(Node *sender) {
    int i;
    int send_id = src_node[sender];
    if (!can_send(sender))
        return false;
    for (i=0; i<pipelines.size(); i++)
        pipelines[i][send_id].back()++;
    return true;
}

int BufferedStaticLink::try_collect_token(Node *receiver) {
    int recv_id;
    int i;
    recv_id = dst_node[receiver];
    for (i=0; i<pipelines[recv_id].size(); i++) {
        if (pipelines[recv_id][i].front()) {
            pipelines[recv_id][i].front()--;
            return 1;
        }
    }
    return 0;
}

void BufferedStaticLink::line_trace(FILE *out) {
    int i, j;
    fprintf(out, "%s:", name.c_str());
//    fprintf(out, "[");
//    for (i=pipeline.size()-1; i>0; i--) {
//        fprintf(out, "%c", pipeline[i] ? '#' : '.');
//    }
//    fprintf(out, "+%c]", pipeline[0] ? '#' : '.');
}

string BufferedStaticLink::to_string() {
    string ret;
    /*
    ret.append(src_node->name + "->");
    ret.append(dst_node->name);
    ret.append(" lat: " + std::to_string(lat) + " type: ");
    if (type == k_ctrl)
        ret.append("ctrl");
    else if (type == k_scal)
        ret.append("scal");
    else if (type == k_vec)
        ret.append("vec");
    else 
        ret.append("unk");
        */
    return ret;
}

void BufferedStaticLink::set_lat(int _lat, int from, int to) {
    int i, j, k;
    printf("Set lat %d from %d to %d\n", _lat, from, to);

    if (lat.size() <= to)
        lat.resize(to+1);
    if (lat[to].size() <= from)
        lat[to].resize(from+1);

    lat[to][from] = _lat;

    pipelines.resize(lat.size());
    for (i=0; i<lat.size(); i++) {
        pipelines[i].resize(lat[i].size());
        for (j=0; j<lat[i].size(); j++) {
            pipelines[i][j].clear();
            for (k=0; k<lat[i][j]; k++) {
                pipelines[i][j].push_back(0);
            }
        }
    }
}


void StaticLink::set_param(string param, string val, vector<int> &index) {
    if (param == "lat") {
        if (index.size() == 2)
            set_lat(atoi(val.c_str()), index[0], index[1]);
        else if (!index.size()) 
            set_lat(atoi(val.c_str()), 0, 0);
        else
            assert(false);
    } else {
        Link::set_param(param, val, index);
    }
}

void StaticLink::clock(uint64_t cycle) {
    int i, j;
    bool stall = false;
    for (i=0; i<pipelines.size(); i++) {
        for (j=0; j<pipelines[i].size(); j++) {
            if (pipelines[i][j].front())
              continue;
            pipelines[i][j].pop_front();
            pipelines[i][j].push_back(false);
        }
    }
}

bool StaticLink::can_send(Node *sender) {
    int i;
    int send_id = src_node[sender];
    bool stall = false;
    for (i=0; i<pipelines.size(); i++)
        stall |= pipelines[i][send_id].back();

    return !stall;
}

bool StaticLink::send(Node *sender) {
    int i;
    int send_id = src_node[sender];
    if (!can_send(sender))
        return false;
    for (i=0; i<pipelines.size(); i++)
        pipelines[i][send_id].back() = true;
    return true;
}

int StaticLink::try_collect_token(Node *receiver) {
    int recv_id;
    int i;
    recv_id = dst_node[receiver];
    for (i=0; i<pipelines[recv_id].size(); i++) {
        if (pipelines[recv_id][i].front()) {
            pipelines[recv_id][i].front() = false;
            return 1;
        }
    }
    return 0;
}

void StaticLink::line_trace(FILE *out) {
    int i, j;
    fprintf(out, "%s:", name.c_str());
//    fprintf(out, "[");
//    for (i=pipeline.size()-1; i>0; i--) {
//        fprintf(out, "%c", pipeline[i] ? '#' : '.');
//    }
//    fprintf(out, "+%c]", pipeline[0] ? '#' : '.');
}

string StaticLink::to_string() {
    string ret;
    /*
    ret.append(src_node->name + "->");
    ret.append(dst_node->name);
    ret.append(" lat: " + std::to_string(lat) + " type: ");
    if (type == k_ctrl)
        ret.append("ctrl");
    else if (type == k_scal)
        ret.append("scal");
    else if (type == k_vec)
        ret.append("vec");
    else 
        ret.append("unk");
        */
    return ret;
}

void StaticLink::set_lat(int _lat, int from, int to) {
    int i, j, k;
    printf("Set lat %d from %d to %d\n", _lat, from, to);

    if (lat.size() <= to)
        lat.resize(to+1);
    if (lat[to].size() <= from)
        lat[to].resize(from+1);

    lat[to][from] = _lat;

    pipelines.resize(lat.size());
    for (i=0; i<lat.size(); i++) {
        pipelines[i].resize(lat[i].size());
        for (j=0; j<lat[i].size(); j++) {
            pipelines[i][j].clear();
            for (k=0; k<lat[i][j]; k++) {
                pipelines[i][j].push_back(false);
            }
        }
    }
}

void NetworkLink::add_src(int id, int addr) {
    if (src_addr.size() <= id) {
      src_addr.resize(id+1, -1);
      delayed_addrs.resize(id+1);
      send_state.resize(id+1, ' ');
    }
    src_addr[id] = addr;
    //std::sort(src_addr.begin(), src_addr.end());
}

void NetworkLink::add_dst(int id, int addr) {
    if (dst_addr.size() <= id) {
      dst_addr.resize(id+1, -1);
      received_bits.resize(id+1, 0);
      recv_state.resize(id+1, ' ');
      classes.resize(id+1, -1);
    }
    dst_addr[id] = addr;
    //std::sort(dst_addr.begin(), dst_addr.end());
}

void NetworkLink::set_param(string param, string val, vector<int> &index) {
    if (param == "saddr") {
        assert(index.size()==1);
        add_src(index[0], atoi(val.c_str()));
    } else if (param == "daddr") {
        assert(index.size()==1);
        add_dst(index[0], atoi(val.c_str()));
    } else if (param == "class") {
      if (index.size() == 0) {
        assert(false);
      } else {
        assert(index.size() == 1);
        classes.at(index[0]) = std::stoi(val);
      }
    } else if (param == "sendvc") {
      cl = std::stoi(val);
    } else if (param == "qdiv") {
      assert(index.size() == 0);
      phit_bits = std::stoi(val);
      printf("Set bits per phit to %d\n", phit_bits);
    } else {
        Link::set_param(param, val, index);
    }
}

bool NetworkLink::can_send(Node *sender) {
    int send_id;
    send_id = src_node[sender];
    assert(src_addr[send_id] >= 0);
    assert(dst_addr[0] >= 0); /* sends to higher addresses are handled in clock. */

    if (delayed_addrs[send_id].size()) {
        //send_state[send_id] = 'Q';
        return false;
    } else if (!net->can_send(src_addr[send_id], cl)) {
        send_state[send_id] = '#';
        return false;
    } else {
        return true;
    }
}

bool NetworkLink::send(Node *sender) {
    int send_id = src_node[sender];
    if (!can_send(sender))
        return false;

    send_state[send_id] = '@';

#ifndef DUPLICATE_FLIT_BROADCAST_HACK
    //if (dst_addr.size() > 1) {
        /* Multiple destinations for this source. */
        for (int i=0; i<dst_addr.size(); i++) {
          for (int j=0; j < bits; j += phit_bits) {
            delayed_addrs[send_id].push(dst_addr[i]);
          }
        }
    //}
    assert(false);
#else
    for (int j=0; j < bits; j += phit_bits) {
      delayed_addrs[send_id].push(0);
    }
#endif
    //printf("Sending from %d from %d, cl%d\n", prog_id, src_addr[send_id], cl);
    net->send(src_addr[send_id], delayed_addrs[send_id].top(), cl, &id, prog_id);
    delayed_addrs[send_id].pop();

    return true;
}

void NetworkLink::clock(uint64_t cycle) {
    void *tmp;
    assert(cl >= 0);
    for (int i=0; i<recv_state.size(); i++)
        recv_state[i] = ' ';
    for (int i=0; i<send_state.size(); i++)
        send_state[i] = ' ';
    for (int i=0; i<delayed_addrs.size(); i++) {
        /* Try entering subsequent destinations with priority in the next 
         * cycles */
        if (!delayed_addrs[i].size()) 
            continue;
        /*if (!net->can_send(delayed_addrs[i][0], cl)) {*/
        if (!net->can_send(src_addr[i], cl)) {
            //send_state[i] = '0' + delayed_addrs[i].size();
            send_state[i] = 'Q';
            continue;
        }
        net->send(src_addr[i], delayed_addrs[i].top(), cl, &id, prog_id);
        send_state[i] = 'q';
        delayed_addrs[i].pop();
    }   
    for (int i=0; i<received_bits.size(); i++) {
      if (received_bits[i] >= bits)
        continue;
      if (!net->recv(dst_addr[i], classes[i], &tmp))
        continue;

      // Received a message at the same address but for a different stream
      if (*(int*)tmp != id) {
          printf("Received id %d, expected %d\n", *(int*)tmp, id);
          assert(0);
      }

      //printf("Received phit (%db) at (%d,%d)\n", phit_bits, dst_addr[i], classes[i]);
      received_bits[i] += phit_bits;
    }
}

int NetworkLink::try_collect_token(Node *receiver) {
    void *tmp;

    int dst_id = dst_node[receiver];
    if (received_bits[dst_id] >= bits) {
      recv_state[dst_id] = '@';
      received_bits[dst_id] = 0;
      return true;
    }

    recv_state[dst_id] = '.';

    return false;

}

void NetworkLink::line_trace(FILE *out) {
    fprintf(out, "%s/", name.c_str());
    for (int i=0; i<send_state.size(); i++)
        fprintf(out, "%c", send_state[i]);
    fprintf(out, "->");
    for (int i=0; i<recv_state.size(); i++)
        fprintf(out, "%c", recv_state[i]);
    fprintf(out, "/");
}
