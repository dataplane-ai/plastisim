#include "func.h"
#include <stdlib.h>
#include <assert.h>

void *
test_fun_vvadd(int n_scal, int n_vec, void **data, int *nagain) {
    float *A;
    float *B;
    int i;

    assert(n_scal == 0);
    assert(n_vec  == 2);

    A = (float*)data[0];
    B = (float*)data[1];

    for (i=0; i<16; i++)
        A[i] += B[i];

    free(data[1]);

    *nagain = 0;
    return data[0];
}

void *
test_fun_dotp(int n_scal, int n_vec, void **data, int *nagain) {
    float *ret;
    int i;
    float *A;
    float *B;

    assert(n_scal == 0);
    assert(n_vec  == 2);

    ret = (float*)malloc(sizeof(float));
    assert(ret);

    A = (float*)data[0];
    B = (float*)data[1];

    *ret = 0;
    for (i=0; i<16; i++)
        *ret += A[i]*B[i];

    free(data[0]);
    free(data[1]);

    *nagain = 0;
    return ret;
}

void *
test_fun_outp(int n_scal, int n_vec, void **data, int *nagain) {
    static float *A = NULL;
    static float *B = NULL;
    static int    pos = 0;
    float *ret;
    int i;

    if (data) {
        A = (float*)data[0];
        B = (float*)data[1];
        pos = 0;
    }
    ret = (float*)malloc(sizeof(float)*16);
    assert(ret);
    for (i=0; i<16; i++)
        ret[i] = A[i]*B[pos];
    pos++;
    *nagain = 16-pos;

    if (pos == 16) {
        free(A);
        free(B);
        A = B = NULL;
    }

    return ret;
}

void *
test_fun_outp_2(int n_scal, int n_vec, void **data, int *nagain) {
    float *A;
    float s;
    assert(n_scal == 1);
    assert(n_vec == 1);
    int i;
    s = *(float*)data[0];
    A = (float*)data[1];

    for (i=0; i<16; i++)
        A[i] *= s;

    *nagain = 0;
    return A;
}

static float my_buf[1024];

void *
test_fun_scal_write(int n_scal, int n_vec, void **data, int *nagain) {
    static int tile_remaining = 32;
    int addr;
    float dat;
    assert(n_scal == 2);
    assert(n_vec == 0);
    addr = *(int*)data[0];
    dat = *(float*)data[1];
    assert(addr >= 0);
    assert(addr < 1024);
    my_buf[addr] = dat;
    *nagain = 0;
    if (tile_remaining--) {
        return NULL;
    } else {
        tile_remaining = 32;
        /* Return garbage token to signal that the tile is done. */
        float *ret = (float*)malloc(sizeof(float));
        assert(ret);
        return ret;
    }
}

void *
test_fun_scal_read(int n_scal, int n_vec, void **data, int *nagain) {
    int addr;
    float *ret;
    assert(n_scal == 1);
    assert(n_vec == 0);
    ret = (float*)malloc(sizeof(float));
    assert(ret);
    addr = *(int*)data[0];
    assert(addr >= 0);
    assert(addr < 1024);
    *ret = my_buf[addr];
    *nagain = 0;
    return ret;
}

