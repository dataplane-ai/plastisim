#include "netshim.h"
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#if __linux__
#include <sys/prctl.h>
#endif

extern InterconnectInterface *g_icnt_interface;
extern int gWriteReqBeginVC;
extern int gWriteReqEndVC;
extern int maxVC;

NetShim::NetShim() {
    char queue_name[100] = "/plastisim.pipe.XXXXXX";
    mktemp(queue_name);

    file     = NULL;
    msg_size = 512;
    max_port = 1;
    max_cl   = 32;
    type     = 0;
    dim      = 0;

}

void NetShim::set_param(string param, string val, vector<int> &idx) {
    int ival = atoi(val.c_str());
    if (param == "cfg") {
        file = strdup(val.c_str());
    } else if (param == "msg_size") {
        msg_size = ival;
    } else if (param == "dim") {
        if (dim && dim != ival) {
            fprintf(stderr, "Network must be square.\n");
            assert(false);
        } 
        dim = ival;
        max_port *= dim;
    } else if (param == "num_classes") {
        max_cl = ival;
    } else {
      assert(false);
    }
}

void NetShim::Finalize() {
    int i;

    max_cl = maxVC+1;

    printf("Number of network classes: %d\n", max_cl);

    eject_buffer_shm = boost::interprocess::mapped_region(
            boost::interprocess::anonymous_shared_memory(
                sizeof(void*)*(max_port*(max_cl+1))));
    std::memset(eject_buffer_shm.get_address(), 0,
                eject_buffer_shm.get_size());
    eject_buffer_ptr = (void **)eject_buffer_shm.get_address();

    inject_buffer_shm = boost::interprocess::mapped_region(
            boost::interprocess::anonymous_shared_memory(
                sizeof(void*)*(max_port*(max_cl+1))));
    std::memset(inject_buffer_shm.get_address(), 0,
                inject_buffer_shm.get_size());
    inject_buffer_ptr = (void **)inject_buffer_shm.get_address();

    dest_buffer_shm = boost::interprocess::mapped_region(
            boost::interprocess::anonymous_shared_memory(
                sizeof(int)*(max_port*(max_cl+1))));
    std::memset(dest_buffer_shm.get_address(), 0,
                dest_buffer_shm.get_size());
    dest_buffer_ptr = (int*)dest_buffer_shm.get_address();

    id_buffer_shm = boost::interprocess::mapped_region(
            boost::interprocess::anonymous_shared_memory(
                sizeof(int)*(max_port*(max_cl+1))));
    std::memset(id_buffer_shm.get_address(), 0,
                id_buffer_shm.get_size());
    id_buffer_ptr = (int*)id_buffer_shm.get_address();

    last_updated.resize(max_port*(max_cl+1), 0);

    char queue_name[100] = "/plastisim.pipe.XXXXXX";
    mktemp(queue_name);
    bool debug = false;
    child_pid = fork();
    if (child_pid) {
        printf("Child process has pid %d\n", child_pid);
        ipc = new IPC(queue_name, true, debug);
    } else {
        printf("I'm the child process!\n");
#if __linux__
        prctl(PR_SET_PDEATHSIG, SIGKILL);
#endif
        ipc = new IPC(queue_name, false, debug);
        remote();
        delete ipc;
        exit(0);
    }
}


uint32_t NetShim::getcnt(uint32_t typ) {
    (void) ipc->read_message();
    assert(ipc->tmp.count.type == IPC::k_type_count 
            && ipc->tmp.count.succ_type == typ);
    return ipc->tmp.count.count;
}

void NetShim::clock_pre(uint64_t cycle) {
    int i, cnt, dst, cl;
    void *data;

    cur_cyc = cycle;

    /* Wait for the remote network to complete the previous cycle. */
    cnt = getcnt(IPC::k_type_ready);

}

void NetShim::clock_post(uint64_t cycle, bool done) {
    int i, cnt;
    /* Tell the network that we're done with the cycle, and it can run. */
    for (i=0; i<max_cl*max_port; i++) {
      if (last_updated[i] + 100 < cycle
          && (inject_buffer_ptr[i] || eject_buffer_ptr[i])) {
        /*if (inject_buffer_ptr[i])
          printf("Possible long injection stall at (%d,%d)\n", i/max_cl, i%max_cl);
        if (eject_buffer_ptr[i])
          printf("Possible long ejection stall at (%d,%d)\n", i/max_cl, i%max_cl);*/
      }
    }
    if (!done) {
        ipc->write_command(IPC::k_op_cont);
    } /*else {
        printf("Shutting down network.\n");
        shutdown();
    }*/
}

bool NetShim::recv(uint32_t dst, uint32_t cl, void **data) {
    assert(cl < max_cl);
    assert(cl >= 0);
    assert(dst < max_port);
    assert(dst >= 0);
    if (!eject_buffer_ptr[dst*max_cl+cl])
        return false;
    //printf("Receiving flit at %d, cl%d!\n", dst, cl);
    last_updated[dst*max_cl+cl] = cur_cyc;
    *data = eject_buffer_ptr[dst*max_cl+cl];
    eject_buffer_ptr[dst*max_cl+cl] = NULL;
    return true;
}

void NetShim::un_recv(uint32_t dst, uint32_t cl, void *data) {
    printf("This shouldn't happen! (%d,%d)\n", dst, cl);
    assert(!eject_buffer_ptr[dst*max_cl+cl]);
    eject_buffer_ptr[dst*max_cl+cl] = data;
}

bool NetShim::can_send(uint32_t src, uint32_t cl) {
    return !inject_buffer_ptr[src*max_cl+cl];
}

bool NetShim::send(uint32_t src, uint32_t dst, uint32_t cl, void *data, int route_id) {
    struct ipc_msg_message msg;
    if (!can_send(src, cl))
        return false;
    last_updated[src*max_cl+cl] = cur_cyc;
    inject_buffer_ptr[src*max_cl+cl] = data;
    dest_buffer_ptr[src*max_cl+cl]    = dst;
    id_buffer_ptr[src*max_cl+cl]    = route_id;
    return true;
}

/* this is the remote part of the shim, that runs in the separate process. */
void NetShim::remote() {
    int i, cnt, node, cl;
    void *tmp;
    icnt = InterconnectInterface::New(file);
    assert(icnt);
    icnt->CreateInterconnect(dim, max_cl);
    icnt->Init();
    g_icnt_interface = icnt;
    gWriteReqBeginVC = 0;
    gWriteReqEndVC = 0;
    //printf("Starting remote loop\n");
    ipc->write_count(cnt, IPC::k_type_ready);
    while (1) {
        while (1) {
            (void) ipc->read_message();
            assert(ipc->tmp.ctrl.type == IPC::k_type_simctrl);
            if (ipc->tmp.ctrl.op == IPC::k_op_cont) {
                break;
            } else if (ipc->tmp.ctrl.op == IPC::k_op_done) {
                //icnt->DisplayStats();
                icnt->DumpFlits();
                //assert(!icnt->Busy());
                delete icnt;
                return;
            }
        }

        /* Then poll the outputs that are available to get their data. */ 
        for (i=0; i<max_port*max_cl; i++)
            if (!eject_buffer_ptr[i])
                eject_buffer_ptr[i] = icnt->Pop(i/max_cl, i%max_cl);

        for (i=0; i<max_port; i++) {
          for (int j=0; j<max_cl; j++) {
            int idx = i*max_cl+j;
            if (inject_buffer_ptr[idx]) {
                if (!icnt->HasBuffer(i, msg_size, j))
                    continue;
                assert(dest_buffer_ptr[idx] >= 0);
                assert(id_buffer_ptr[idx] >= 0);
                icnt->Push(i, dest_buffer_ptr[idx], inject_buffer_ptr[idx],
                        msg_size, j, id_buffer_ptr[idx]);
                inject_buffer_ptr[idx] = NULL;
                dest_buffer_ptr[idx] = -1;
                id_buffer_ptr[idx] = -1;
            }
          }
        }
#ifdef DEBUG
        printf("R: Advancing network.\n");
#endif
        icnt->Advance();
        ipc->write_count(cnt, IPC::k_type_ready);
    }
}

void NetShim::shutdown() {
    printf("Sending shutdown IPC message.\n");
    ipc->write_command(IPC::k_op_done);
    waitpid(child_pid, NULL, 0);
    delete ipc;
}

