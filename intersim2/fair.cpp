#include "fair.hpp"
#include "assert.h"

StrictFairArbiter::StrictFairArbiter() {
  buf_size = 0;
}

void StrictFairArbiter::Disable() {
  buf_size = 0;
}

bool StrictFairArbiter::CanGo(int flow_id) {
  if (batch.find(flow_id) != batch.end())
    return true;
  /* Allow up to buf_size copies of batch through at a given time. */
  return allowed_already[flow_id] < buf_size*batch[flow_id];
}

void StrictFairArbiter::Going(int flow_id) {
  assert(CanGo(flow_id));
  if (batch.find(flow_id) != batch.end())
    return;
  allowed_already[flow_id]++;
}

void StrictFairArbiter::Clock() {
  /* If we haven't allowed a full batch through yet, return. */
  for (auto it = batch.begin(); it != batch.end(); it++)
    if (allowed_already[it->first] < it->second)
      return;
  /* We've allowed a full batch; decrement to allow another. */
  for (auto it = batch.begin(); it != batch.end(); it++)
    allowed_already[it->first] -= it->second;
}

void StrictFairArbiter::AddItemToBatch(int flow_id, int count) {
  assert(batch.find(flow_id) != batch.end());
  batch[flow_id] = count;
}

void StrictFairArbiter::SetBatchDifference(int size) {
  assert(size>0);
  buf_size = size;
}

