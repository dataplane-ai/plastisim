#ifndef _FAIR_HPP_
#define _FAIR_HPP_

#include <map>

using std::map;

class StrictFairArbiter {
public:
  bool CanGo(int flow_id);
  void Going(int flow_id);
  void Clock();
  void AddItemToBatch(int flow_id, int count);
  void SetBatchDifference(int size);
  void Disable();
  StrictFairArbiter();
private:
  int buf_size;
  map<int,int> batch;
  map<int,int> allowed_already;
};

#endif

