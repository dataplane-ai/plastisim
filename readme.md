Plastisim: A Fast Simulator for Reconfigurable Arrays
========================================================================
Plastisim is a token passing simulator designed to efficiently simulate reconfigurable arrays. It supports:
 * pipelines, 
 * memories, 
 * fixed-latency links, and 
 * an arbitrary number of networks on chip.

Plastisim takes the following options:
 * -f config: This is a mandatory option specifying the configuration file to run.
 * -t: This option enables printing a cycle by cycle trace of selected simulation elements.
 * -d dir: This results in print dot graph files for each cycle in dir. 
 * -c cycles: This overrides the default cycle limit of 100,000 cycles.

Pipeline Nodes
------------------------------------------------------------------------
Pipeline nodes form the core of Plastisim's simulation model. The basic pipeline node takes input tokens and produces output tokens after a defined latency. The pipeline model has a global stall signal, that stalls the pipeline if any output is not able to receive. The following options are supported, and configure the behavior of the entire node:
 * lat: This sets the latency of the pipeline.
 * process: This sets the model for simulating DRAM. Valid options include:
     - fixed: This takes oone integer parameter, which is the number of cycles between completed DRAM transactions.
     - bernoulli: This takes one parameter between 0 and 1, which is the probability that the DRAM completes a transaction in a given cycle.
     - onoff: This takes three parameters. The first is the likelihood that the DRAM turns on if it is off, the second is the likelihood that the DRAM turns off if it's on, and the third is the likelihood that the DRAM completes a transaction while it's on.
 * trace: Whether the node should appear in the line trace. Default is to not appear.
 * start_at_tokens: The number of tokens that the node starts with. Typically used for the input arguments to the accelerator, to allow it to run exactly once.
 * stop_after_tokens: The number of tokens that a node must receive to stop the simulation. Typically used to stop after the simulation is complete.

Plastisim also supports several indexed parameters for pipeline nodes:
 * proc_param: This sets the process parameters for the selected DRAM process. Typically, only the first process parameter is used, but onoff requires more.
 * scale_in: This sets the input scaling factor for each input. To a single network message being responsible for multiple activations of a given CU, each received token can be scaled on the receive end by this factor.
 * scale_out: To accurately model reductions within the PCU, it is necessary to provide a factor that requires multiple pipeline activations per output token. scale_out counts a given number of pipeline tokens before sending a single output token.
 * buffer: This allows the size of the input buffers to be configured. The units are in unscaled (network) tokens, and must be at least one.

Links
------------------------------------------------------------------------
Static links are contention-free, fixed latency interconnects. They have a single source and destination node, and a single pipeline with a global backpressure signal. They are defined using the name "link" and the following parameters:
 * lat: The latency of the link. Must be >= 1.

Network links are defined in reference to a booksim network. They extend the idea of a static link by allowing each logical link (source node to destination node) to be mapped to a physical link on the network. They use the name "netlink" and have the following additional params:
 * net: This is the mandatory network name (see below).
 * saddr: This is the integer address of the injection port for this traffic entering the network. 
 * daddr: This is the integer address of the destination port for this traffic.
 * class: This is the traffic class of traffic in the network. Currently, it is taken as the index of the destination link. For example, if a destination link is the first link connecting to the destination node, it will have class 0.

All links share a common set of parameters:
 * src: The source node for the link.
 * dst: The destination node for the link.
 * type: The type of the link. Either vec, scal, or ctrl. Currently unused.
 * trace: zero to disable tracing (default), non-zero to enable. 

Networks
------------------------------------------------------------------------
Booksim networks are specified by using the "net" name, followed on the same line by the path to the booksim configuration file. This is 

Memories
------------------------------------------------------------------------

