#include "parse.h"
#include <string>
#include <fstream>
#include <iostream>
#include <ctype.h>
#include <stdlib.h>

#include "link.h"
#include "netshim.h"

#include "routefunc.hpp"

using std::ifstream;
using std::getline;
using std::cout;

extern int maxVC;
extern map<pair<int,int>, vector<pair<char,int>>> gGlobalRoutes;
map<pair<int,int>, int> classMap;
map<int, int> q_map;
extern int nonprio_vc_slow;

void trim(string& str) {
    int f, l;
    f = str.find_first_not_of(" \t\r\n\v");
    l = str.find_last_not_of(" \t\r\n\v");
    str = str.substr(f, l-f+1);
}

bool get_next_useful_line(ifstream& in, string& line) {
    int firstch;
    while (1) {
        if (in.eof()) 
            return false;
        getline(in, line);
        firstch = line.find_first_not_of(" \r\n\t\v");
        if (firstch == string::npos)
            continue;
        if (line[firstch] == '#')
            continue;
        firstch = line.find_first_of("#");
        if (firstch != string::npos)
            line = line.substr(0, firstch);
        return true;
    }
}

bool try_parse_kv_line(ifstream& in, string& key, string& value, vector<int>& indices) {
    string line;
    int splpos;
    int lbkt, rbkt;
    int pos;
    //if (!isspace(in.peek()))
        //return false;

    pos  = in.tellg();
    get_next_useful_line(in, line);
    // If it doesn't start with whitespace, return fail
    if (!isspace(line[0])) {
        in.clear();
        in.seekg(pos);
        return false;
    }

    // Split on the first equal sign
    splpos = line.find_first_of("=");
    if (splpos == string::npos) {
        fprintf(stderr, "Invalid KV line: %s\n", line.c_str());
        exit(1);
    }

    key = line.substr(0, splpos);
    value = line.substr(splpos+1);

    // Trim leading/trailing whitespace from key, value, return success
    trim(key);
    trim(value);

    indices.resize(0);
    // Look for a square bracket in the line
    lbkt = key.find_first_of("[");
    rbkt = key.find_last_of("]");
    if (lbkt != string::npos && rbkt != string::npos) {
        int ipos = 0;
        string indexstr = key.substr(lbkt+1);
        while (ipos != string::npos) {
            //std::cout << indexstr << std::endl;
            indices.push_back(atoi(indexstr.c_str()));
            ipos = indexstr.find_first_of(",");
            indexstr = indexstr.substr(ipos+1, -1);
        }
        assert(rbkt == key.size()-1);
        key = key.substr(0, lbkt);
    }

    //printf("%s=%s ", key.c_str(), value.c_str());
    //for (auto x: indices)
    //  printf(" %d", x);
    //printf("\n");
    
    return true;
}

bool try_parse_decl_line(ifstream& in, string& type, string& name, string& param) {
    string line;
    int splpos;

    // Get the line
    if (!get_next_useful_line(in, line))
        return false;
    if (isspace(line[0]))
        return false;

    // Split on the first space
    splpos = line.find_first_of(" \t\r\n\v");
    if (splpos == string::npos) {
        fprintf(stderr, "Invalid type-name line: %s\n", line.c_str());
        exit(1);
    }

    type = line.substr(0, splpos);
    name = line.substr(splpos+1);

    // Trim leading/trailing whitespace from key, value, return success
    trim(type);
    trim(name);
    param = string();
    splpos = name.find_first_of(" \t\r\n\v");
    if (splpos == string::npos) {
        /*printf("Type: %s Name: %s\n", type.c_str(), name.c_str());*/
        return true;
    }
    param = name.substr(splpos+1);
    name = name.substr(0, splpos);

    printf("Type: %s Name: %s Param: %s\n", type.c_str(), name.c_str(), param.c_str());
    
    return true;
}

bool parse_node(ifstream& in, Node *node, SymTab<Link>& linktab, SymTab<MemWrap>& dramtab) {
    string key, val;
    vector<int> idx;
    int linkid;
    while (try_parse_kv_line(in, key, val, idx)) {
        if (key == "link_in") {
            assert(idx.size() == 1);
            Link *link = linktab.get_ptr(val);
            node->set_in_link(idx[0], link);
        } else if (key == "link_out") {
            assert(idx.size() == 1);
            Link *link = linktab.get_ptr(val);
            node->set_out_link(idx[0], link);
        } else if (key == "controller") {
            assert(idx.size() == 0);
            DRAMNode *as_dram = dynamic_cast<DRAMNode*>(node);
            assert(as_dram);
            as_dram->SetController(dramtab.get_ptr(val));
        } else if (idx.size() == 1) {
            node->set_indexed_param(key, val, idx[0]);
        } else if (idx.size() == 0) {
            node->set_param(key, val);
        } else {
            assert(false);
        }
    }
    node->Finalize();
    return true;
}

bool parse_net(ifstream& in, NetShim *net) {
    string key, value;
    vector<int> idx;
    while (try_parse_kv_line(in, key, value, idx)) {
        net->set_param(key, value, idx);
    }
    net->Finalize();
    return true;
}

void parse_placement(ifstream& in, map<int,int>& net_map, 
    map<int,int>& vc_map, map<int,map<int,pair<string,int>>>& hop_map,
    /* Map from route_id to map of <src,dst> to lat. */
    map<int,map<pair<int,int>,int>> &stat_map) {
    int id, addr, vc, dst, lat;
    char type;
    char dir;
    while (in >> type >> id >> addr) {
      switch (type) {
        case 'N':
          /*printf("Adding mapping from %d to %d.\n", id, addr);*/
          net_map[id] = addr;
          break;
        case 'V':
          /*printf("Adding vc from %d to %d.\n", id, addr);*/
          vc_map[id] = addr;
          if (addr > maxVC)
            maxVC = addr;
          gInjectVC[id] = addr;
          break;
        case 'Q':
          q_map[id] = addr;
          break;
        case 'H':
          in >> dir >> vc;
          /*printf("Adding hop for route %d at %d (dir %c, vc %d).\n", id, addr, 
              dir, vc);*/
          if (vc > maxVC)
            maxVC = vc;
#ifdef DUPLICATE_FLIT_BROADCAST_HACK
          if (find(gGlobalRoutes[make_pair(addr,id)].begin(),
                   gGlobalRoutes[make_pair(addr,id)].end(),
                   make_pair(dir, vc))
              ==   gGlobalRoutes[make_pair(addr,id)].end()) {
#endif
            gGlobalRoutes[make_pair(addr,id)].push_back(make_pair(dir, vc));
#ifdef DUPLICATE_FLIT_BROADCAST_HACK
          }
#endif
          if (dir == 'X')
            classMap[make_pair(addr,id)] = vc;


          gRoutePosition[make_pair(addr,id)] = 0;
          if (hop_map[addr][id].first.find(dir) == string::npos)
            hop_map[addr][id].first.push_back(dir);
          hop_map[addr][id].second = vc;
          break;
        case 'S':
          in >> dst >> lat;
          // Need to iterate over links, and find the one that has
          // the route ID. Then, need to find the indices of the source node
          // by iterating over src_node, and comparing prog_id. Repeat for
          // dest_node, then execute set_lat.
          printf("Adding static hop for route %d (%d->%d, lat %d).\n", id,
              addr, dst, lat);
          stat_map[id][make_pair(addr,dst)] = lat;
          break;
      }
    }
    printf("Printing hop routes\n");
    for (const auto& addr: hop_map) {
      printf("%d", addr.first);
      for (const auto& route: addr.second) {
        auto hop = route.second;
        printf("\t%d: %s, vc%d\n", route.first, hop.first.c_str(), hop.second);
      }
    }
}

bool parse_mc(ifstream& in, MemWrap *mem) {
    string key, value;
    vector<int> idx;
    while (try_parse_kv_line(in, key, value, idx)) {
        if (idx.size() > 0) {
            assert(false);
        } else {
            mem->set_param(key, value);
        }
    }
    mem->Finalize();
    return true;
}

bool parse_link(ifstream& in, Link *link, SymTab<Node>& nodetab, SymTab<NetShim>& nettab, map<int,int>& net_map, map<int,int>& vc_map, map<int,map<pair<int,int>,int>>& stat_map, int flit_width) {
    string key, value;
    vector<int> idx;
    vector<int> src_ids, dst_ids;
    vector<int> daddrs;
    vector<int> daddrs_idx;
    int route_id = -1;
    MultiLinkAdapter *nl = dynamic_cast<MultiLinkAdapter*>(link);
    bool static_convert = false;
    // Src_id -> index
    // Dst_id -> index
    while (try_parse_kv_line(in, key, value, idx)) {
      int ival = atoi(value.c_str());
        /* Utility functions to cooperate with the placer. */
        if (key == "src_id") {
            assert(net_map.count(atoi(value.c_str())));
            link->set_param("saddr", std::to_string(net_map[atoi(value.c_str())]), idx);
            src_ids.push_back(ival);
        } else if (key == "dst_id") {
            assert(net_map.count(atoi(value.c_str())));
            link->set_param("daddr", std::to_string(net_map[atoi(value.c_str())]), idx);
            dst_ids.push_back(ival);
            daddrs.push_back(net_map[ival]);
            daddrs_idx.push_back(idx[0]);
        } else if (key == "vc_id") {
            route_id = ival;
            if (vc_map.find(ival) != vc_map.end()) {
              vector<int> tmp;
              nl->set_network();
              if (q_map.find(ival) != q_map.end()) {
                fprintf(stdout, "Setting quality factor slowdown of %d for %d\n",
                    q_map[ival] ? nonprio_vc_slow : 1, ival);
                link->set_param("qdiv", std::to_string(q_map[ival] ? flit_width/nonprio_vc_slow : flit_width), tmp);
              } else {
                fprintf(stdout, "Could not find quality factor for link %d!\n", ival);
              }
              link->set_param("sendvc", std::to_string(vc_map[ival]), tmp);
              //link->set_param("class", std::to_string(vc_map[route_id]), idx);
              link->set_param("prog_id", value, idx);
            } else {
              if (stat_map.find(ival) == stat_map.end()) {
                fprintf(stderr, "Could not find link ID %d in placement file.\n",
                    ival);
              }
              assert(stat_map.find(ival) != stat_map.end());
              nl->set_static();
              static_convert = true;
            }
        } else if (!key.compare("src")) {
            if(!idx.size())
                link->set_src_node(nodetab.get_ptr(value), 0);
            else if (idx.size() == 1)
                link->set_src_node(nodetab.get_ptr(value), idx[0]);
            else
                assert(false);
        } else if (!key.compare("dst")) {
            if(!idx.size())
                link->set_dst_node(nodetab.get_ptr(value), 0);
            else if (idx.size() == 1)
                link->set_dst_node(nodetab.get_ptr(value), idx[0]);
            else
               assert(false);
        } else if (key == "net") {
            nl->set_network(nettab.get_ptr(value));
        } else {
            link->set_param(key, value, idx);
        }
    }
    for (int i=0; i<daddrs.size(); i++) {
      assert(route_id >= 0);
      vector<int> tmp_idx {daddrs_idx[i]};
      link->set_param("class", std::to_string(classMap[make_pair(daddrs[i], route_id)]), tmp_idx);
      printf("Set ejection VC at addr %d to %d\n", daddrs[i], classMap[make_pair(daddrs[i], route_id)]);
    }
    if (route_id >= 0) {
      printf("Link specified in placement file.\n");
      if (static_convert) {
        printf("Changing link to static with placed latencies.\n");
        idx = {0, 0};
        for (int f: src_ids) {
          for (int t: dst_ids) {
            int lat;
            lat = stat_map[route_id].at(make_pair(f,t));
            nl->set_param("lat", std::to_string(lat), idx);
            idx[1]++;
          }
          idx[1] = 0;
          idx[0]++;
        }
      } else {
        printf("Updating dynamic link exit VCs.\n");
        for (const auto &pos: gGlobalRoutes) {
          if (pos.first.second == route_id) {
            printf("%d hops for route %d\n", pos.second.size(), pos.first.second);
            for (const auto &rt: pos.second) {
              if (rt.first == 'X') {
                int pos_idx = 
                  std::find(daddrs.begin(), daddrs.end(), pos.first.first) - daddrs.begin();
                assert(pos_idx < daddrs.size());
                vector<int> tmp {pos_idx};
                printf("Set exit class for pos %d to %d (rt %d)\n", pos_idx, rt.second, route_id);
                link->set_param("class", std::to_string(rt.second), tmp);
              }
            }
          }
        }
      }
    }
    return true;
}

bool parse_add_names(ifstream& in, char statictype, int flit_width, SymTab<NetShim>& nettab, SymTab<Link>& linktab, SymTab<Node>& nodetab, SymTab<MemWrap>& dramtab) {
    string type, name;
    string param;
    int id;
    // Parse the first line of a data structure to figure out type and name
    while (1) {
        while (!try_parse_decl_line(in, type, name, param))
            if (in.eof())
                goto done;
        // Based on the type, create a new appropriate SymTab entry and call the right other parser function
        if (!type.compare("link")) {
            StaticLink *link = new StaticLink();
            if ((id = linktab.add_name_get_id(name, link)) < 0)
                assert(false);
            //printf("Added id %d for name %s\n", id, name.c_str());
        } else if (!type.compare("netlink")) {
            MultiLinkAdapter *link = new MultiLinkAdapter(statictype, flit_width);
            link->set_network();
            if ((id = linktab.add_name_get_id(name, link)) < 0)
                assert(false);
        } else if (!type.compare("net")) {
            NetShim *net = new NetShim();
            if ((id = nettab.add_name_get_id(name, net)) < 0)
                assert(false);
        } else if (!type.compare("mc")) {
            MemWrap *mem = new MemWrap();;
            if ((id = dramtab.add_name_get_id(name, mem)) < 0)
                assert(false);
        } else if (!type.compare("node")) {
            Node *node = new PipeNode();
            if ((id = nodetab.add_name_get_id(name, node)) < 0)
                assert(false);
        } else if (!type.compare("memnode")) {
            Node *node = new MemNode();
            if ((id = nodetab.add_name_get_id(name, node)) < 0)
                assert(false);
        } else if (type == "dramnode") {
            Node *node = new DRAMNode();
            if ((id = nodetab.add_name_get_id(name, node)) < 0)
                assert(false);
        } else {
            //printf("Unknown type: %s\n");
            assert(false);
        }
    }
done:
    in.clear();
    in.seekg(0);
    return true;
}

bool parse_all(ifstream& in, SymTab<NetShim>& nettab, SymTab<Link>& linktab, SymTab<Node>& nodetab, SymTab<MemWrap>& dramtab, map<int,int>& net_map, map<int,int>& vc_map,
    map<int,map<pair<int,int>,int>> &stat_map, int flit_width) {
    string type, name;
    string param;
    int id;
    // Parse the first line of a data structure to figure out type and name
    while (1) {
        while (!try_parse_decl_line(in, type, name, param))
            if (in.eof())
                goto done;
        if (type == "link" || type == "netlink") {
            id = linktab.get_name_id(name);
            if (!parse_link(in, linktab.get_ptr(id), nodetab, nettab, net_map, vc_map, stat_map, flit_width))
                assert(false);
        } else if (type == "net") {
            parse_net(in, nettab.get_ptr(name));
        } else if (type == "mc") {
            id = dramtab.get_name_id(name);
            if (!parse_mc(in, dramtab.get_ptr(id)))
                assert(false);
        } else if (type == "node" || type == "memnode" || type == "dramnode") {
            id = nodetab.get_name_id(name);
            if (!parse_node(in, nodetab.get_ptr(id), linktab, dramtab))
                assert(false);
        } else {
            fprintf(stderr, "Unknown type: %s\n", type.c_str());
            assert(false);
        }
    }
done:
    return true;
}
