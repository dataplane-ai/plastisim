#include "memwrap.h"

using namespace DRAMSim;

#define TRANS_SIZE 64

int MemWrap::getCallbackID(DRAMNode *node) {
    callbacks.push_back(node);
    return callbacks.size()-1;
}

bool MemWrap::addReq(bool is_write, uint64_t addr, int id) {
    if (is_write) {
        pend_write.insert(std::make_pair(addr, id));
    } else {
        pend_read.insert(std::make_pair(addr, id));
    }
    return mem->addTransaction(is_write, addr);
}
   
void MemWrap::readRet(unsigned id, uint64_t addr, uint64_t cyc) {
    //printf("Returning read to addr %d\n", addr);
    int cnt;
    cnt = pend_read.count(addr);
    assert(cnt);
    if (cnt == 1) {
        /* If the count is one, call the only callback for that address. */
        callbacks[pend_read.find(addr)->second]->token_return(addr);
        pend_read.erase(addr);
    } else {
        /* Otherwise, find the first callback and call it. */
        auto it = pend_read.begin();
        for (; it != pend_read.end(); it++) {
          if (it->first == addr) {
            callbacks[it->second]->token_return(addr);
            pend_read.erase(it);
            break;
          }
        }
    }
    readB += TRANS_SIZE;
}

void MemWrap::print_activity(FILE *out, uint64_t cycle) {
  fprintf(out, "Total %s:\t %6.2f GB/s (%6.2f GB/s R, %6.2f GB/s W)\n",
      name.c_str(),
      (float)(readB+writeB)/cycle, (float)readB/cycle, (float)writeB/cycle);
  fprintf(out, "Pending Read: %d\t Pending Write: %d\t\n",
      pend_read.size(), pend_write.size());
}

void MemWrap::set_param(string param, string val) {
    int ival = atoi(val.c_str());
    if (param == "memfile") {
        memfile = val;
    } else if (param == "sysfile") {
        sysfile = val;
    } else if (param == "tracename") {
        tracename = val;
    } else if (param == "memsize") {
        memsize = ival;
    } else {
        assert(false);
    }
}

void MemWrap::Finalize() {
    TransactionCompleteCB *ReadCallback;
    TransactionCompleteCB *WriteCallback;

    assert(!memfile.empty());
    assert(!sysfile.empty());

    ReadCallback = new Callback<MemWrap, void, unsigned, uint64_t, uint64_t>
        (this, &MemWrap::readRet);
    WriteCallback = new Callback<MemWrap, void, unsigned, uint64_t, uint64_t>
        (this, &MemWrap::writeRet);

    if (tracename.empty())
        tracename = name;
    mem = getMemorySystemInstance(
            memfile, sysfile, rootdir, tracename, memsize);
    mem->setCPUClockSpeed(1000*1000*1000 /* TODO: change back. 1 MHz */);
    mem->RegisterCallbacks(ReadCallback, WriteCallback, NULL);
}
   
void MemWrap::writeRet(unsigned id, uint64_t addr, uint64_t cyc) {
    //printf("Returning write to addr %d\n", addr);
    int cnt;
    cnt = pend_write.count(addr);
    assert(cnt);
    if (cnt == 1) {
        /* If the count is one, call the only callback for that address. */
        callbacks[pend_write.find(addr)->second]->token_return(addr);
        pend_write.erase(addr);
    } else {
        /* Otherwise, find the first callback and call it. */
        auto it = pend_write.begin();
        for (; it != pend_write.end(); it++) {
          if (it->first == addr) {
            callbacks[it->second]->token_return(addr);
            pend_write.erase(it);
            break;
          }
        }
    }
    writeB += TRANS_SIZE;
}

void MemWrap::clock(uint64_t cycle) {
    mem->update();
}

bool MemWrap::ready() {
    return mem->willAcceptTransaction();
}
