#ifndef _IPC_HPP_
#define _IPC_HPP_

#include <fcntl.h>
#include <sys/stat.h>
//#include <mqueue.h>
#include <stdint.h>

#include <boost/interprocess/ipc/message_queue.hpp>

using namespace boost::interprocess;

struct ipc_msg_count {
    uint32_t type;
    uint32_t count;
    uint32_t succ_type;
};

struct ipc_msg_ready {
    uint32_t type;
    uint32_t node;
    uint32_t cl;
    uint32_t ready_for;
};

struct ipc_msg_message {
    uint32_t type;
    uint32_t src;
    uint32_t dst;
    uint32_t cl;
    void*    data;
};

struct ipc_msg_simctrl {
    uint32_t type;
    uint32_t op;
};


union ipc_msg {
    struct ipc_msg_count   count;
    struct ipc_msg_ready   ready;
    struct ipc_msg_message message;
    struct ipc_msg_simctrl ctrl;
};

class IPC {
    public:
        IPC(const char *name, bool host, bool _debug = false);
        ~IPC();
        void write_count(uint32_t count, uint32_t succ_type);
        void write_ready(uint32_t node, uint32_t cl, uint32_t ready_for = 1);
        void write_command(uint32_t op);
        void write_message(uint32_t src, uint32_t dst, uint32_t cl, void *data);
        static const int k_type_count   = 1;
        static const int k_type_ready   = 2;
        static const int k_type_message = 3;
        static const int k_type_simctrl = 4;

        static const int k_op_cont = 1;
        static const int k_op_done = 2;

        union ipc_msg *read_message();
        bool debug;
        bool is_host;
        union ipc_msg tmp;
    private:
        uint64_t tot_send;
        uint64_t tot_recv;
        void asserte(bool cond, const char *msg = "");
        void print_msg_in_buf(bool send = true);
        char namebuf[255];
        int child;
        message_queue *mq_send;
        message_queue *mq_recv;
};

#endif
