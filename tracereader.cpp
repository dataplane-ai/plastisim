#include "tracereader.h"
#include <assert.h>

void TraceReader::SetFile(string file) {
    is_static = false;
    trace.open(file);
    assert(trace.is_open());
    Advance();
}
uint64_t TraceReader::val() {
    return cur_val;
}
void TraceReader::SetStatic(uint64_t val) {
    cur_val = val;
    is_static = true;
}
void TraceReader::SetGeneric(string val) {
    if (val.find_first_not_of("0123456789 ") == string::npos) {
        SetStatic(atoi(val.c_str()));
    } else {
        SetFile(val);
    }
}
void TraceReader::Advance() {
    if (is_static)
        return;
    assert(!trace.eof());
    trace >> cur_val;
}
