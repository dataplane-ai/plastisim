#ifndef LINK_H_
#define LINK_H_

#include <deque>
#include <string>
#include <map>
#include <stdexcept>

#include "node.h"
#include "netshim.h"
#include "symtab.h"

using std::deque;
using std::priority_queue;
using std::string;
using std::map;

class Link {
    friend class Node;
    friend class MultiLinkAdapter;
    friend class SymTab<Link>;
protected:
    int           id;
    int           expected_count;
    enum tok_type type;
    int bits;
public:
    int           prog_id;
    map<Node*,int> src_node;
    map<Node*,int> dst_node;
    string        name;
    bool          trace;
    virtual void line_trace(FILE *out) = 0;
    virtual string to_string()  { return string(); }
    virtual bool can_send(Node* sender) = 0;
    virtual bool send(Node *sender) = 0;
    virtual void credit(Node *receiver) = 0;
    virtual void clock(uint64_t cycle) = 0;
    virtual int  try_collect_token(Node *receiver) = 0;
    virtual void set_param(string param, string val, vector<int> &index);
    virtual void set_src_node(Node *src, int idx) { src_node[src] = idx; }
    virtual void set_dst_node(Node *dst, int idx) { dst_node[dst] = idx; }
                 //Link(int _id, string _name) : id(_id), name(_name) {}
                 Link() { trace = false; }
};

class StaticLink : public Link {
private:
    // first vector dimension is to, then from.
    // therefore, the pipeline from a to b is pipelines[b][a]
    vector<vector<
        deque<bool>>>   pipelines;
    virtual void          set_lat(int _lat, int from, int to);
    // first vector dimension is to, then from.
    vector<vector<int>> lat;
public:
    virtual void credit(Node *receiver) {}
    virtual void line_trace(FILE *out);
    virtual bool can_send(Node* sender);
    virtual bool send(Node* sender);
    virtual void clock(uint64_t cycle);
    virtual int  try_collect_token(Node* receiver);
    virtual void set_param(string param, string val, vector<int> &index);

    virtual string to_string(); 
};

class CreditLink : public Link {
private:
    vector<int> lat;
    // first vector dimension is to, then from.
    vector<deque<bool>>   pipelines_data;
    vector<deque<int>>   pipelines_credits;
    void          set_lat(int _lat, int from, int to);
    /* First dimension is from, then to. */
    vector<int> credits;
public:
    void line_trace(FILE *out) {}
    bool can_send(Node* sender);
    bool send(Node* sender);
    void credit(Node *receiver);
    void clock(uint64_t cycle);
    int  try_collect_token(Node* receiver);
    void set_param(string param, string val, vector<int> &index);
};


class BufferedStaticLink : public Link {
private:
    int buf_depth {2};
    // first vector dimension is to, then from.
    // therefore, the pipeline from a to b is pipelines[b][a]
    vector<vector<
        deque<int>>>   pipelines;
    void          set_lat(int _lat, int from, int to);
public:
    virtual void credit(Node *receiver) {}
    // first vector dimension is to, then from.
    vector<vector<int>> lat;
    void line_trace(FILE *out);
    bool can_send(Node* sender);
    bool send(Node* sender);
    void clock(uint64_t cycle);
    int  try_collect_token(Node* receiver);
    void set_param(string param, string val, vector<int> &index);

    string to_string(); 
};

class NetworkLink : public Link {
private:
    void add_src(int id, int addr);
    void add_dst(int id, int addr);
    int           cl;
    int           flit_bits;
    int           phit_bits;
    NetShim      *net;
    vector<char>  recv_state;
    vector<char>  send_state;
    vector<int>   classes;
    vector<int>   received_bits;
    /* The vector is per source, then a list of destinations. */
    vector<priority_queue<int>> delayed_addrs;
public:
    virtual void credit(Node *receiver) {}
    vector<int>   src_addr;
    vector<int>   dst_addr;
    void line_trace(FILE *out); 
    bool can_send(Node *sender);
    bool send(Node *sender);
    void clock(uint64_t cycle); 
    int  try_collect_token(Node *receiver);
    void set_param(string param, string val, vector<int> &index);
    void set_network(NetShim *_net) { net = _net; }
    NetworkLink(int _flit_bits) {
        printf("Constructing NetworkLink.\n");
        cl = -1;
        flit_bits = _flit_bits;
        phit_bits = _flit_bits;
    }

    //string to_string(); 

};

class MultiLinkAdapter : public Link {
  Link *stat_l;
  NetworkLink *net_l;
  Link *active;
public:
  MultiLinkAdapter(char stype, int flit_bits) {
    if (stype == 'B') {
      stat_l = new BufferedStaticLink();
    } else if (stype == 'S') {
      stat_l = new StaticLink();
    } else if (stype == 'C') {
      stat_l = new CreditLink();
    } else {
      assert(false);
    }
    net_l = new NetworkLink(flit_bits);
    active = net_l;
  }
  void set_static() { 
    active = stat_l; 
    active->name = name; 
    active->id = id;
  }
  void set_network() { 
    active = net_l;
    active->name = name; 
    active->id = id;
  }
  void set_src_node(Node *src, int idx) {
    stat_l->set_src_node(src, idx);
    net_l->set_src_node(src, idx);
  }
  void set_dst_node(Node *dst, int idx) {
    stat_l->set_dst_node(dst, idx);
    net_l->set_dst_node(dst, idx);
  }
  void line_trace(FILE *out) {
    active->name = name;
    active->line_trace(out);
  }
  bool can_send(Node *sender) {
    return active->can_send(sender);
  }
  void credit(Node *receiver) {
    active->credit(receiver);
  }
  bool send(Node *sender) {
    return active->send(sender);
  }
  void clock(uint64_t cycle) {
    active->clock(cycle);
  }
  int  try_collect_token(Node *receiver) {
    return active->try_collect_token(receiver);
  }
  void set_param(string param, string val, vector<int> &index) {
    if (param == "trace") {
      if (val == "on" || val == "1") trace = true;
      else if (val == "off" || val == "0") trace = false;
      else assert(false);
      return;
    }
    int set_in = 2;
    try {
      stat_l->set_param(param, val, index);
    } catch (std::invalid_argument e) {
      set_in --;
      /* Not all parameters are valid. */
    }
    try {
      net_l->set_param(param, val, index);
    } catch (std::invalid_argument e) {
      set_in --;
      /* Not all parameters are valid. */
    }
    /* We should set the parameter in at least one sublink. */
    if (set_in == 0)
      throw std::invalid_argument(param + ": " + val);
  }
  void set_network(NetShim *_net) { net_l->set_network(_net); }
};

#endif

