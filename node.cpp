#include "node.h"
#include "link.h"
#include "memwrap.h"

#include <bitset>

#define DRAM_MAX_BURST 64
#define DRAM_BURST_MASK (~(64-1))

dest_spec::dest_spec() {
    link= NULL;
    scale.SetStatic(1);
    unscaled_tokens = 0;
}

string dest_spec::to_string() {
    string ret;
    ret.append(link->name);
    return ret;
}

bool input_spec::is_space() { 
    return ((!!scaled_tokens)+unscaled_tokens) <= buffer_depth; 
}

bool input_spec::is_enough() { 
    if (!scaled_tokens && unscaled_tokens) {
        pending_credits++;
        scaled_tokens += scale.val();
        total_unscaled_tokens += scale.val(); // TODO: Yaqi this seems counting total scaled token?
        unscaled_tokens--;
        scale.Advance();
    }
    return scaled_tokens > 0; 
}

void input_spec::poll_tokens(int count) { 
  assert(pending_credits >= 0);
  if (pending_credits) {
    while (pending_credits--) {
      //printf("Node %s return credit to link.\n", node->name.c_str());
      link->credit(node);
    }
    pending_credits = 0;
  }
  unscaled_tokens += link->try_collect_token(node); 
}

input_spec::input_spec() {
    link = NULL;
    node = NULL;
    count = 1;
    scale.SetStatic(1);
    buffer_depth = 4;
    pending_credits = 4;
    scaled_tokens = 0;
    unscaled_tokens = 0;
    total_unscaled_tokens = 0;
}

string input_spec::to_string() {
    string ret;
    ret.append(link->name + " *"+std::to_string(scale.val())
            +" /"+std::to_string(count));
    return ret;
}

PipeNode::PipeNode() {
    set_lat(1);
}

//Node::Node(int _id, string _name) : id(_id), name(_name) {
Node::Node() {
    id = 0;
    expected_count = 0;
    trace=true;
    proc = k_fixed;
    proc_params.push_back(1);
    proc_state = 1;
    scale_out = 1;
    initial_tokens = 0;
    required_tokens = 0;
    start_cycle = 0;
    pipestall = pipestarve = true;
    total_outputs = active_cycles = stalled_cycles = starved_cycles = 0;
    timeout = 0;
}

bool Node::timed_out() {
  return timeout > 10000;
}

void Node::print_activity(FILE *out, uint64_t cycle) {
  const char *status = 
    //expected_count >= active_cycles ? "" :
    active_cycles >= expected_count ? "DONE" :
    !pipestall && !pipestarve ? "ACTIVE" :
                  pipestall && !pipestarve ? "STALL" :
                 !pipestall &&  pipestarve ? "STARVE" : "BOTH";

    fprintf(out, "%s: Active: %5.1f Stalled: %5.1f Starved: %5.1f Total Active: %8lld Total Output: %8lld Expected Active: %8lld Last Activity: %d Final State: %s", 
        name.c_str(),
        100.0*active_cycles/cycle,
        100.0*stalled_cycles/cycle,
        100.0*starved_cycles/cycle, 
        active_cycles,
        total_outputs,
        expected_count,
        timeout,
        status);
    fprintf(out, " Input State: ");
    for (auto& i: inputs)
      fprintf(out, "%c", i.is_enough() ? '@' : '.');
    fprintf(out, " Output State: ");
    for (auto& d: dests)
      fprintf(out, "%c", d.unscaled_tokens == d.scale.val() && !d.link->can_send(this) ? '.' : '@');
    fprintf(out, "\n\t");
    for (const auto &i: inputs) {
      fprintf(out, "%8lld(%lld, %lld)", i.total_unscaled_tokens, i.unscaled_tokens, i.scaled_tokens);
    }
    fprintf(out, "\n");
}

string Node::to_string() {
    string ret;
    for (int i=0; i<inputs.size(); i++)
        ret.append("[" + inputs[i].to_string() + "] ");
    ret.append(string(" => "));
    for (int i=0; i<dests.size(); i++)
        ret.append("[" + dests[i].to_string() + "] ");

    return ret;
}

bool Node::is_satisfied() {
    return !required_tokens;
}
/*
void MemNode::line_trace(FILE *out) {
    int i;
    fprintf(out, "%s:", name.c_str());
    for (i=0; i<inputs.size(); i++)
        fprintf(out, "(%03ds)", inputs[i].scaled_tokens);
    fprintf(out, "[");
    fprintf(out, "%1d/%d", written_partitions_to_read/total_partitions);
    fprintf(out, "]");
}
*/

void Node::line_trace(FILE *out) {
    fprintf(out, "<%s:", short_name.c_str());
    for (auto& i: inputs)
      fprintf(out, "%c", i.is_enough() ? '@' : '.');
    fprintf(out, ":%c>", pipestall && !pipestarve ? 'T' :
                         pipestall &&  pipestarve ? '#' :
                        !pipestall &&  pipestarve ? 'R' :
                        !pipestall && !pipestarve ? '@' : 'x');
}

void Node::line_vcd_header(FILE *out) {
    const char* sn = short_name.c_str();
    const char* fn = name.c_str();
    fprintf(out, "$scope module %s $end\n", sn);
    for (int idx=0; idx < inputs.size(); ++idx) {
        fprintf(out, "$var int 32 %s_in%d_us %s_%s_us $end\n", sn, idx, sn, inputs[idx].link->name.c_str());
    }
    fprintf(out, "$var wire 1 %s_stall %s_stall $end\n", sn, sn);
    fprintf(out, "$var wire 1 %s_starve %s_starve $end\n", sn, sn);
    fprintf(out, "$var int 64 %s_count %s_count $end\n", sn, sn);
    fprintf(out, "$upscope $end\n");
}

void Node::line_vcd(FILE *out) {
    for (int idx=0; idx < inputs.size(); ++idx) {
        string unscaled = std::bitset<32>(inputs[idx].unscaled_tokens).to_string();
        fprintf(out, "b%s %s_in%d_us\n", unscaled.c_str(), short_name.c_str(), idx);
    }
    fprintf(out, "b%d %s_stall\n", pipestall, short_name.c_str());
    fprintf(out, "b%d %s_starve\n", pipestarve, short_name.c_str());
    string acycle = std::bitset<64>(active_cycles).to_string();
    fprintf(out, "b%s %s_count\n", acycle.c_str(), short_name.c_str());
}
/*
void PipeNode::line_trace(FILE *out) {
    int i;
    fprintf(out, "%s:", name.c_str());
    for (i=0; i<inputs.size(); i++)
        fprintf(out, "(%03ds)", inputs[i].scaled_tokens);
    fprintf(out, "[");
    for (i=populated_pipeline_stages.size()-1; i>=0; i--) {
        assert(populated_pipeline_stages[i] <= 1);
        if (populated_pipeline_stages[i])
            fprintf(out, "#");
        else
            fprintf(out, ".");
    }
    //fprintf(out, "%1d", populated_pipeline_stages[0]);
    fprintf(out, "]");
}
*/
void Node::resize_node_in(int idx) {
    int i;
    if (inputs.size() <= idx)
        inputs.resize(idx+1);
    for (i=0; i<inputs.size(); i++)
        inputs[i].node = this;
}

void Node::resize_node_out(int idx) {
    if (dests.size() <= idx)
        dests.resize(idx+1);
}

void Node::clock(uint64_t cycle) {
    int i;
    pipestall = false;
    pipestarve = false;
    if (cycle < start_cycle)
      pipestall = true;
    /* Start by sending outputs from all destinations that can send. If any
     * destination buffer is full and backpressured from sending, then set the
     * stall signal. */
    for (i=0; i<dests.size(); i++) {
        if (dests[i].unscaled_tokens == dests[i].scale.val()) {
            if (dests[i].link->can_send(this)) {
                dests[i].link->send(this);
                dests[i].unscaled_tokens = 0;
                dests[i].scale.Advance();
            } else {
                pipestall = true;
            }
        }
    }
    /* Then receive input buffers from the network as applicable. */
    for (i=0; i<inputs.size(); i++) {
        if (!inputs[i].is_enough())
            pipestarve = true;
        if (inputs[i].is_space())
            inputs[i].poll_tokens(1);
    }

    if (inputs.size() == 0)
        pipestarve = true;

    if (initial_tokens)
        pipestarve = false;

    if (pipestarve)
      starved_cycles++;
    if (pipestall)
      stalled_cycles++;

    timeout++;
    if (!pipestall && !pipestarve) {
      active_cycles++;
      timeout = 0;
    }
}

void Node::consume_inputs() {
    int i;
    if (initial_tokens)
        initial_tokens--;
    else  {
        for (i=0; i<inputs.size(); i++)
            inputs[i].scaled_tokens--;
        for (i=0; i<inputs.size(); i++)
          assert(inputs[i].scaled_tokens >= 0);
    }
}

void Node::produce_output() {
    int i;
    total_outputs++;
    if (required_tokens)
        required_tokens--;
    else
        for (i=0; i<dests.size(); i++)
            dests[i].unscaled_tokens++;
}

bool Node::get_dram_status(uint64_t cycle) {
    bool dram_go = false;
    switch (proc) { /* Based on https://github.com/booksim/booksim2/blob/master/src/injection.cpp */
        case k_fixed:
            assert(proc_params.size() == 1);
            dram_go = (cycle % (int) proc_params[0]) == 0;
            break;
        case k_bernoulli:
            assert(proc_params.size() == 1);
            dram_go = (rand()*1.0 / RAND_MAX) < proc_params[0];
            break;
        case k_onoff:
            assert(proc_params.size() == 3);
            proc_state = proc_state ? (rand()*1.0/RAND_MAX < proc_params[0])
                                    : (rand()*1.0/RAND_MAX < proc_params[1]);
            dram_go = proc_state && (rand()*1.0/RAND_MAX < proc_params[2]);
            break;
        default:
            assert(false);
    }
    return dram_go;
}

void MemNode::clock(uint64_t cycle) {
    int i;
    bool full;
    full = false;
    assert(written_partitions_to_read + !!reading_partition_tokens <= total_partitions);
    if (written_partitions_to_read + !!reading_partition_tokens == total_partitions)
        full = true;
    Node::clock(cycle);
    /* Next, try to perform a write into the buffer. */
    if (!pipestarve && writing_partition_tokens < tile_size && !full) {
        Node::consume_inputs();
        writing_partition_tokens++;
    }
    /* Finally, try to perform a read from the buffer. */
    if (!pipestall && reading_partition_tokens) {
        Node::produce_output();
        reading_partition_tokens--;
    }
    /* Update the buffers after all reads/writes to prevent information 
     * traveling within a single cycle. */
    if (!reading_partition_tokens && written_partitions_to_read) {
        reading_partition_tokens = tile_size;
        written_partitions_to_read--;
    }
    if (writing_partition_tokens == tile_size) {
        writing_partition_tokens = 0;
        written_partitions_to_read++;
    }
}

void PipeNode::clock(uint64_t cycle) {
    int i;
    Node::clock(cycle);
    if (!get_dram_status(cycle))
        return;

    /* If the send pipe stall signal is asserted, then do not advance the
     * pipeline and return immediately. Otherwise, advance the pipeline by one
     * step. */
    if (pipestall)
        return;

    if (populated_pipeline_stages[0])
        Node::produce_output();

    populated_pipeline_stages.pop_front();
    if (pipestarve) {
        populated_pipeline_stages.push_back(false);
    } else {
        Node::consume_inputs();
        populated_pipeline_stages.push_back(true);
    }
}

void Node::set_in_link(int idx, Link *link) {
    resize_node_in(idx);
    inputs[idx].link = link;
    vector<int> dummy_idx;
#if 0 // This is a hack
    if (typeid(*link) == typeid(NetworkLink)) {
        link->set_param("class", std::to_string(idx), dummy_idx);
    }
#endif
}

void Node::set_out_link(int idx, Link *link) {
    resize_node_out(idx);
    dests[idx].link = link;
}

void Node::set_indexed_param(string param, string val, int idx) {
    int   ival = atoi(val.c_str());
    float fval = atof(val.c_str());

    if (param == "scale_in") {
        resize_node_in(idx);
        inputs[idx].scale.SetGeneric(val);
    } else if (param == "release_token_at") {
        start_cycle = std::stoi(val);
    } else if (param == "unscaled_init_token") {
        resize_node_in(idx);
        inputs[idx].unscaled_tokens = ival;
    } else if (param == "scale_out") {
        resize_node_out(idx);
        dests[idx].scale.SetGeneric(val);
    //} else if (param == "link_recv_idx") {
    //    resize_node_in(idx);
    //    inputs[idx].link_recv_idx = ival;
    //} else if (param == "link_send_idx") {
    //    resize_node_out(idx);
    //    dests[idx].link_send_idx = ival;
    } else if (param == "count_in") {
        resize_node_in(idx);
        inputs[idx].count = ival;
    } else if (param == "proc_param") {
        if (proc_params.size() <= idx)
            proc_params.resize(idx+1);
        proc_params[idx] = fval;
    } else if (param == "buffer") {
        resize_node_in(idx);
        inputs[idx].buffer_depth = ival;
        inputs[idx].pending_credits = ival;
    } else {
        fprintf(stderr, "Could not set node option, %s[%d] = %s\n",
                param.c_str(), idx, val.c_str());
        assert(false);
    }

}
/*
void DRAMNode::line_trace(FILE *out) {
}
*/

DRAMNode::DRAMNode() {
    mem = NULL;
    is_write = false;
    tok_size = 4;
    in_tok_size = 64;
    burst_size = 64;
    readB = writeB = 0;
    rob_entries = 256;
    remain_burst = 0;
}

void DRAMNode::print_activity(FILE *out, uint64_t cycle) {
  Node::print_activity(out, cycle);
  printf("\tDRAM: %6.2f GB/s (%6.2f GB/s R, %6.2f GB/s W)\n",
    (float)(readB+writeB)/cycle, (float)readB/cycle, (float)writeB/cycle);
  printf("\tROB remaining: %d\n", rob.size());
}

void DRAMNode::SetController(MemWrap *_mem) {
    mem = _mem;
    cb_idx = mem->getCallbackID(this);
}

void DRAMNode::token_return(uint64_t addr) {
  int returned = 0;
  for (auto &trans: rob) {
  /*  printf("Compare %lu against %lu in DRAM return: ",
        std::get<0>(trans)&DRAM_BURST_MASK,
        addr&DRAM_BURST_MASK);*/
    if ((std::get<0>(trans)&DRAM_BURST_MASK) == (addr&DRAM_BURST_MASK)) {
      //printf("\tDRAM return match %d~%d at %s\n", std::get<0>(trans), addr, name.c_str());
      std::get<1>(trans) = true;
      if (is_write)
        writeB += std::get<2>(trans);
      else
        readB += std::get<2>(trans);
      returned++;
    }
    //printf("\n");
  }
  /* Should not return unrequested transaction. */
  if (!returned)
    fprintf(stdout, "WARNING: Unrequested DRAM token returned at %s: %d!\n", name.c_str(), addr);
  /*assert(returned);*/
}

void DRAMNode::set_param(string param, string val) {
    if (param == "offset_trace" || param == "addr_trace") {
        trace_file.SetGeneric(val);
    } else if (param == "size_trace") {
        size_file.SetGeneric(val);
    } else if (param == "out_token_size") {
        tok_size = atoi(val.c_str());
        //burst_size = tok_size;
    } else if (param == "in_token_size") {
        in_tok_size = atoi(val.c_str());
        //burst_size = in_tok_size;
    } else if (param == "rob_entries") {
        rob_entries = atoi(val.c_str());
    } else if (param == "burst_size") {
        burst_size = atoi(val.c_str());
        assert(burst_size <= DRAM_MAX_BURST);
    } else if (param == "dram_cmd_tp") {
        if (val == "dense_load") {
            is_write = false;
        } else if (val == "dense_store") {
            is_write = true;
        } else if (val == "sparse_load") {
            is_write = false;
            /* Set the sparse traces to have a constant size. */
            size_file.SetStatic(4);
        } else if (val == "sparse_store") {
            is_write = true;
            size_file.SetStatic(4);
        } else {
            assert(false);
        }
    } else {
        Node::set_param(param, val);
    }
}

void DRAMNode::clock(uint64_t cycle) {
    /* Handle the inputs/outputs first. */
    Node::clock(cycle);

    if (rob.size() && !pipestall) {
      auto& rob_head = rob.front();
      if (std::get<1>(rob_head) == true) {
        Node::produce_output();
        if (is_write)
          std::get<2>(rob_head) -= in_tok_size;
        else
          std::get<2>(rob_head) -= tok_size;
        //printf("Node %s trans. dequeue stall.\n", name.c_str());
      }
      if (std::get<2>(rob_head) <= 0) {
        rob.pop_front();
      }
    }

    /* If the memory won't accept a transaction, end cycle. */
    if (!mem->ready()) {
      if (!pipestall && !pipestarve)
        active_cycles--;
      pipestall = true;
      //printf("Node %s DRAM stall.\n", name.c_str());
      return;
    }
    /* Burst finished, move on to next trace entry. */
    if (remain_burst <= 0) {
      if (pipestarve) {
          return;
      }
      next_addr = trace_file.val();
      remain_burst = size_file.val();

      //printf("Node %s advance mem trace.\n", name.c_str());

      trace_file.Advance();
      size_file.Advance();
    }

    if (rob.size() >= rob_entries) {
      if (!pipestall && !pipestarve)
        active_cycles--;
      pipestall = true;
      //printf("Node %s ROB stall.\n", name.c_str());
      return;
    }

    if (pipestall || pipestarve)
      return;

    /* If the inputs are all ready, debit the tokens, and send a memory msg. */
    Node::consume_inputs();
    //printf("Node %s issue %s.\n", name.c_str(), is_write ? "write" : "read");

    /* Add the request. */
    bool sent = false;
    for (auto &trans: rob) {
      if ((std::get<0>(trans)&DRAM_BURST_MASK) == (next_addr&DRAM_BURST_MASK)) {
        sent = true;
        break;
      }
    }
    if (!sent) {
      assert(mem->addReq(is_write, next_addr&DRAM_BURST_MASK, cb_idx));
      //fprintf(stdout, "Send DRAM request at %s: %d!\n", name.c_str(), next_addr&DRAM_BURST_MASK);
    }

    /* Add a re-order buffer entry. */
    rob.push_back(std::make_tuple(next_addr&DRAM_BURST_MASK, sent, burst_size));
    //fprintf(stdout, "Add DRAM ROB entry at %s: %d!\n", name.c_str(), next_addr&DRAM_BURST_MASK);

    /* Prepare for the next burst. */
    next_addr += burst_size;
    remain_burst -= burst_size;
}


MemNode::MemNode() {
    total_partitions = 1;
    writing_partition_tokens = 0;
    reading_partition_tokens = 0;
    written_partitions_to_read = 0;
}

void MemNode::set_param(string param, string val) {
    int ival = atoi(val.c_str());
    if (param == "partitions") {
        total_partitions = ival;
    } else if (param == "tile_size") {
        tile_size = ival;
    } else {
        Node::set_param(param, val);
    }
}

void PipeNode::set_param(string param, string val) {
    int ival = atoi(val.c_str());
    if (param == "lat") {
        set_lat(ival);
    } else {
        Node::set_param(param, val);
    }
}

void Node::set_param(string param, string val) {
    int ival = atoi(val.c_str());
    if (param == "count") {
        expected_count = ival;
    } else if (param == "start_at_tokens") {
        initial_tokens = ival;
    } else if (param == "stop_after_tokens") {
        required_tokens = ival;
    } else if (param == "process") {
        if      (val == "fixed")     proc = k_fixed;
        else if (val == "bernoulli") proc = k_bernoulli;
        else if (val == "onoff")     proc = k_onoff;
        else                         goto fail;
    } else if (param == "trace") {
        trace = ival ? true : false;
    } else {
        goto fail;
    }

    return;
fail:
    fprintf(stderr, "Could not set node option, %s = %s\n",
            param.c_str(), val.c_str());
    assert(false);


}

void PipeNode::set_lat(int _lat) {
    int i;
    lat = _lat;
    populated_pipeline_stages.clear();
    for (i=0; i<lat; i++)
        populated_pipeline_stages.push_back(0);
}

void Node::Finalize() {
  short_name = "";
  for (const auto c: name)
    if (!( c >= 'a' && c <= 'z'))
      short_name.push_back(c);
}

