#!/usr/bin/perl

use warnings;
use strict;

#print "Opening input file $ARGV[0]\n";
open IN, $ARGV[0] or die $!;

my $lastlink = "";
my $lastnode = "";

my %inlinknames;
my %outlinknames;
my %inlinkscales;
my %outlinkscales;

my %linkscaleout;
my %linkscalein;
my %linksrcs;
my %linkdsts;

my $latency;

sub finalize_node {
    foreach (keys %inlinknames) {
        #print "$inlinknames{$_} -> $inlinkscales{$_}\n";
        $linkscalein{$inlinknames{$_}} = $inlinkscales{$_};
    }
    foreach (keys %outlinknames) {
        $linkscaleout{$outlinknames{$_}} = $outlinkscales{$_};
    }
    %inlinknames = ();
    %outlinknames = ();
    %inlinkscales = ();
    %outlinkscales = ();
    if (not $latency) {
        $latency = 1;
    }
    print "$lastnode [shape=box, label=\"$lastnode\\nlat=$latency\"];\n";
    $latency = "";
}

print "digraph {\n";

while (<IN>) {
    if ($_ =~ /node ([^ ]*)/) {
        if ($lastnode) {
            finalize_node();
        }
        $lastnode = $1;
    } elsif ($_ =~ /link ([^ ]*)/) {
        $lastlink = $1;
    } elsif ($_ =~ /src = ([^ ]*)/) {
        $linksrcs{$lastlink} = $1;
    } elsif ($_ =~ /dst = ([^ ]*)/) {
        $linkdsts{$lastlink} = $1;
    } elsif ($_ =~ /lat = ([^\s]*)/) {
        $latency = $1;
    } elsif ($_ =~ /link_in\[([0-9]+)\] = ([^\s ]*)$/) {
        #print "Link in $1 -> $2\n";
        $inlinknames{$1} = $2;
    } elsif ($_ =~ /link_out\[([0-9]+)\] = ([^\s ]*)$/) {
        $outlinknames{$1} = $2;
    } elsif ($_ =~ /scale_in\[([0-9]+)\] = ([^\s ]*)$/) {
        $inlinkscales{$1} = $2;
    } elsif ($_ =~ /scale_out\[([0-9]+)\] = ([^\s]*)$/) {
        $outlinkscales{$1} = $2;
    }
}

finalize_node();

foreach (keys %linksrcs) {
    #print "$linksrcs{$_} -> $linkdsts{$_} [label=\"$_ (*$linkscalein{$_}/$linkscaleout{$_})\"];\n";
    if (not exists $linkscalein{$_}) {
        $linkscalein{$_} = 1;
    }
    if (not exists $linkscaleout{$_}) {
        $linkscaleout{$_} = 1;
    }
    print "$linksrcs{$_} -> $linkdsts{$_} [label=\"$_ (/$linkscaleout{$_}*$linkscalein{$_})\"];\n";
}

print "}\n";
