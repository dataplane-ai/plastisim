#ifndef PARSE_H
#define PARSE_H

#include <fstream>
#include <map>

#include "netshim.h"
#include "node.h"
#include "symtab.h"
#include "memwrap.h"

using std::map;
using std::ifstream;

bool parse_all(ifstream& in, SymTab<NetShim>& nettab, SymTab<Link>& linktab, SymTab<Node>& nodetab, SymTab<MemWrap>& dramtab, map<int,int>& net_map, map<int,int>& vc_map, map<int,map<pair<int,int>,int>>& stat_map, int flit_width);
bool parse_add_names(ifstream& in, char statictype, int flit_width, SymTab<NetShim>& nettab, SymTab<Link>& linktab, SymTab<Node>& nodetab, SymTab<MemWrap>& dramtab);
void parse_placement(ifstream& in, map<int,int>& net_map, map<int,int>& vc_map, map<int,map<int,pair<string,int>>>& hop_map, map<int,map<pair<int,int>,int>>& stat_map);


#endif

