#ifndef NETSHIM_H
#define NETSHIM_H

#include <vector>
#include <queue>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <boost/interprocess/anonymous_shared_memory.hpp>
#include <boost/interprocess/mapped_region.hpp>

#include "node.h"
#include "ipc.hpp"

using std::vector;
using std::queue;

class NetShim {
    const char *file;
    int         msg_size;
    int         max_port;
    int         max_cl;
    int         type;
    int         dim;
    short       child_pid;
    uint64_t    cur_cyc;

    InterconnectInterface *icnt;
    IPC *ipc;

    boost::interprocess::mapped_region eject_buffer_shm;
    void **eject_buffer_ptr;
    boost::interprocess::mapped_region inject_buffer_shm;
    void **inject_buffer_ptr;
    boost::interprocess::mapped_region dest_buffer_shm;
    int *dest_buffer_ptr;
    boost::interprocess::mapped_region id_buffer_shm;
    int *id_buffer_ptr;

    vector<uint64_t> last_updated;
                 
    queue<ipc_msg_message> pending_msgs; 
    vector<void*> pushed_back_data;

    void remote();
    uint32_t getcnt(uint32_t typ);

    friend class SymTab<NetShim>;
protected:
    int id;
    string name;

public:
    NetShim();
    void Finalize();
    void set_param(string param, string val, vector<int> &idx);
    bool send(uint32_t src, uint32_t dst, uint32_t cl, void *data, int route_id);
    bool can_send(uint32_t src, uint32_t cl);
    bool recv(uint32_t dst, uint32_t cl, void **data);
    void un_recv(uint32_t dst, uint32_t cl, void* data);
    void clock_pre(uint64_t cycle);
    void clock_post(uint64_t cycle, bool done);
    void shutdown();
    ~NetShim() {}

};


#endif

