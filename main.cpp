#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <vector>
#include <stdint.h>
#include <assert.h>
#include <unistd.h>
#include <string>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <signal.h>
#include <execinfo.h>
#include <map>

#include "link.h"
#include "symtab.h"
#include "parse.h"
#include "print.h"
#include "ipc.hpp"
#include "memwrap.h"

using std::vector;
using std::string;
using std::map;

SymTab<NetShim> nettab;
SymTab<Link> linktab;
SymTab<Node> nodetab;
SymTab<MemWrap> dramtab;
map<int,int> net_map;
map<int,int> vc_map;
map<int,map<int,pair<string,int>>> hop_map;
map<int,map<pair<int,int>,int>> stat_map;
int maxVC;
int nonprio_vc_slow;

void chld_handler(int sig) {
    assert(sig == SIGCHLD);
    sleep(1); /* Allow the child to print a backtrace. */
    printf("Exiting after child termination.\n");
    assert(false);
}

void trace(bool all, uint64_t cycle) {
  int i;
  fprintf(stdout, "%6lu: ", cycle);
  for (i=0; i<linktab.count(); i++) {
      if (!linktab.get_ptr(i)->trace && !all)
          continue;
      linktab.get_ptr(i)->line_trace(stdout);
      fprintf(stdout, " ");
  }
  for (i=0; i<nodetab.count(); i++) {
      if (!nodetab.get_ptr(i)->trace && !all)
          continue;
      nodetab.get_ptr(i)->line_trace(stdout);
      fprintf(stdout, " ");
  }
  fprintf(stdout, "\n");
}


void segv_handler(int sig) {
    /* From backtrace man page  and
     * https://stackoverflow.com/questions/15129089/is-there-a-way-to-dump-stack-trace-with-line-number-from-a-linux-release-binary */
    if (!getppid())
        return;
    int j, nptrs;
    void *buffer[128];
    char scom[256];
    char **strings;

    nptrs = backtrace(buffer, 128);
    printf("[bt] Backtrace", nptrs);

    strings = backtrace_symbols(buffer, nptrs);
    for (j=1; strings && j<nptrs; j++) {
        printf("[bt] %s: ", strings[j]);
        sprintf(scom, "addr2line %p -e plastisim\n", buffer[j]);
        system(scom);
        printf("\n");
    }

    free(strings);
    exit(-1);
}

void change_sim_dir(const char *config) {
    string dir = string(config);
    size_t found;
    found = dir.find_last_of("/");
    if (found != string::npos)
        dir = dir.substr(0, found);
    else
        dir = string();
    printf("Changing to config directory: %s\n", dir.c_str());
    chdir(dir.c_str());
}

int main(int argc, char **argv) {
    FILE *dotout;
    srand(0);
    uint64_t max_cycle, cycle;
    struct sigaction act;
    int opt, verbose;
    char *infile, *dotfile, *placefile, *vcd;
    bool all_done;
    bool do_trace;
    int i;
    int deadlock_warnings;
    size_t pos;
    char statictype;
    int flit_width;
    memset(&act, 0, sizeof(act));
    ifstream instream;
    ifstream placestream;
    verbose = 0;
    infile = NULL;
    dotfile = NULL;
    placefile = NULL;
    vcd = NULL;
    max_cycle = -1;
    cycle = 0;
    do_trace = false;
    maxVC = 0;
    deadlock_warnings = 0;
    statictype='S';
    flit_width = 512;
    nonprio_vc_slow = 1;
    act.sa_handler = segv_handler;
    //sigaction(SIGSEGV, &act, NULL);
    while (-1 != (opt = getopt(argc, argv, "q:f:vc:td:p:w:l:i:"))) {
        switch (opt) {
            case 'i':
                flit_width = atoi(optarg);
                break;
            case 'v':
                verbose = 1;
                break;
            case 'w':
                vcd = optarg;
                break;
            case 'f':
                infile = optarg;
                break;
            case 'c':
                max_cycle = atoi(optarg);
                break;
            case 'd':
                dotfile = optarg;
                break;
            case 't':
                do_trace = true;
                break;
            case 'l':
                statictype=optarg[0];
                break;
            case 'p':
                placefile = optarg;
                break;
            case 'q':
                printf("Set non-priority VC slowdown to %d\n",
                    atoi(optarg));
                nonprio_vc_slow = atoi(optarg);
                break;
            default:
                fprintf(stderr, "Unknown argument!\n");
                exit(1);
        }
    }
    instream.open(infile);
    if (!instream.is_open()) {
        fprintf(stderr, "Unable to open input file!\n");
        exit(1);
    }
    change_sim_dir(infile);
    parse_add_names(instream, statictype, flit_width, nettab, linktab, nodetab, 
        dramtab);
    printf("Added all names to the symbol table.\n");
    if (placefile) {
        placestream.open(placefile);
        parse_placement(placestream, net_map, vc_map, hop_map, stat_map);
        printf("Parsed placement file.\n");
    }
    parse_all(instream, nettab, linktab, nodetab, dramtab, net_map, vc_map, stat_map, flit_width);
    printf("Parsed definitions of all elements.\n");
    print_all(stdout, linktab, nodetab);
    printf("\n");
    act.sa_handler = chld_handler;
    //sigaction(SIGCHLD, &act, NULL);

    FILE * vcdFile = NULL;
    /* VCD Header */
    if (vcd) {
        vcdFile = fopen ("wave.vcd","w");
        fprintf(vcdFile, "$timescale 1ns $end\n");
        for (i=0; i<nodetab.count(); i++) {
            if (!nodetab.get_ptr(i)->trace)
                continue;
            nodetab.get_ptr(i)->line_vcd_header(vcdFile);
        }
        fprintf(vcdFile, "$enddefinitions $end\n");
        fprintf(vcdFile, "$dumpvars $end\n");
    }
    while (cycle++ < max_cycle) {
        if (do_trace) {
            trace(false, cycle);
            /* ------ VCD Generation BEGIN ------*/
            if (vcdFile) {
              fprintf(vcdFile, "#%d \n", cycle);
              for (i=0; i<nodetab.count(); i++) {
                  if (!nodetab.get_ptr(i)->trace)
                      continue;
                  nodetab.get_ptr(i)->line_vcd(vcdFile);
              }
            }
            /* ------ VCD Generation END ------*/
            if (dotfile) {
                char dotnamebuf[256];
                snprintf(dotnamebuf, 256, "%s/frame%07d.dot", dotfile, cycle);
                dotout = fopen(dotnamebuf, "w");
                if (dotout)
                    print_dot(dotout, linktab, nodetab, cycle);
                else
                    fprintf(stderr, "Could not open dot output file: %s\n", dotnamebuf);
                fclose(dotout);
            }
        } else if (cycle % 10000 == 0) {
            printf("%d...\n", cycle);
            fflush(stdout);
        }
        //printf("Pre-clocking networks.\n");
        for (i=0; i<nettab.count(); i++)
            nettab.get_ptr(i)->clock_pre(cycle);
        // Simulate all links
        //printf("Clocking links.\n");
        for (i=0; i<linktab.count(); i++)
            linktab.get_ptr(i)->clock(cycle);
        // Simulate all DRAMs
        for (i=0; i<dramtab.count(); i++)
            dramtab.get_ptr(i)->clock(cycle);
        // Simulate all nodes
        //printf("Clocking nodes.\n");
        for (i=0; i<nodetab.count(); i++)
            nodetab.get_ptr(i)->clock(cycle);
        // Check done condition
        all_done = true;
        for (i=0; i<nodetab.count(); i++)
            all_done &= nodetab.get_ptr(i)->is_satisfied();
            //if (!nodetab.get_ptr(i)->is_satisfied())
                //all_done = false;
        //printf("Post-clocking networks.\n");
        for (i=0; i<nettab.count(); i++)
            nettab.get_ptr(i)->clock_post(cycle, all_done);
        if (all_done)
            break;
        all_done = true;
        for (i=0; i<nodetab.count(); i++)
          all_done &= nodetab.get_ptr(i)->timed_out();
        if (all_done) {
          trace(true, cycle);
          if (deadlock_warnings++ > 50)  {
            printf("POSSIBLE DEADLOCK:\n");
            /*print_dot(stdout, linktab, nodetab, cycle);*/
            break;
          }
        }
    }
    printf("Simulation complete at cycle: %lu%s\n", cycle, cycle > max_cycle ? " (TIMED OUT)" : "");
    /* VCD Ending */
    if (vcdFile) {
        fclose(vcdFile);
    }
    /* Print out the final activity statistics. */
    for (i=0; i<nodetab.count(); i++)
      nodetab.get_ptr(i)->print_activity(stdout, cycle);
    for (i=0; i<dramtab.count(); i++)
      dramtab.get_ptr(i)->print_activity(stdout, cycle);
    signal(SIGCHLD, SIG_DFL);
    sleep(1);
    for (i=0; i<nettab.count(); i++)
      nettab.get_ptr(i)->shutdown();
    /* Put a trailing segment on the animation. */
    for (int i=1; i<10; i++) {
        if (dotfile) {
            char dotnamebuf[256];
            snprintf(dotnamebuf, 256, "%s/frame%07d.dot", dotfile, cycle+i);
            dotout = fopen(dotnamebuf, "w");
            if (dotout)
                print_dot(dotout, linktab, nodetab, cycle);
            else
                fprintf(stderr, "Could not open dot output file: %s\n", dotnamebuf);
            fclose(dotout);
        }
    }
    fflush(stdout);
    return 0;
}

