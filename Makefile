# Adapted from the booksim2 makefile
BOOK_DIR  = intersim2
DRAM_DIR  = DRAMSim2
DRAM_LIB  = libdramsim.a
BOOK_SRCS = $(wildcard $(BOOK_DIR)/*.cpp) $(wildcard $(BOOK_DIR)/*/*.cpp)
BOOK_SRCS2 = $(filter-out intersim2/main.cpp,$(BOOK_SRCS))
BOOK_OBJS = $(BOOK_SRCS:%.cpp=%.o)
CPP_SRCS = $(wildcard *.cpp)
CPP_HDRS = $(wildcard *.h)
CPP_OBJS = $(CPP_SRCS:%.cpp=build/%.o)
CPP_DEPS = $(CPP_SRCS:%.cpp=build/%.d)

OBJS := $(CPP_OBJS) 

CPP = g++
CPP_FLAGS = -std=c++11 -I/opt/local/include  \
	    -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include \
	    -g -O3 -I$(BOOK_DIR) -I$(DRAM_DIR) #-DDEBUG=1 
CPP_FLAGS += -DDUPLICATE_FLIT_BROADCAST_HACK
LD_FLAGS = -lrt -lpthread # -L./DRAMSim2 -ldramsim
LD_FLAGS_MAC = -lpthread
UNAME_S := $(shell uname -s)

PROG = plastisim
BUILD_DIR = build

all: $(PROG)

$(PROG): $(OBJS) $(DRAM_DIR)/$(DRAM_LIB)
	make -j 8 -e SIM_OBJ_FILES_DIR=../build -C intersim2 all 
	#echo $(BOOK_OBJS)
ifeq ($(UNAME_S),Darwin)
	$(CPP) $^ -o $@ intersim2/*.o intersim2/*/*.o $(DRAM_DIR)/$(DRAM_LIB) $(LD_FLAGS_MAC) 
else
	$(CPP) $^ -o $@ intersim2/*.o intersim2/*/*.o $(DRAM_DIR)/$(DRAM_LIB) $(LD_FLAGS) 
endif

$(DRAM_DIR)/$(DRAM_LIB):
	sh -c "cd DRAMSim2; make $(DRAM_LIB)"

clean:
	make -C intersim2 clean
	rm -f plastisim
	rm -f $(CPP_OBJS) $(CPP_DEPS)

build/%.o: %.cpp
	$(CPP) $(CPP_FLAGS) -MMD -c $< -o $@

ctag:
	ctags --languages=c++ -f .tags -R . 

-include $(CPP_DEPS)
