#ifndef PRINT_H
#define PRINT_H

#include "link.h"
#include "symtab.h"
#include "node.h"

void print_all(FILE *out, SymTab<Link>& linktab, SymTab<Node>& nodetab);
void print_dot(FILE *out, SymTab<Link>& linktab, SymTab<Node>& nodetab, uint64_t cycle);

#endif
