#!/bin/bash

ROOT=/home/acrucker/finals3
PLACE=v1s4

#for PLACE in 1.S4.QLimUltra1k_3.place v2s4.place v0s0.place v0s4.place
OUT=finals3_results_3buf0rtdelay_$PLACE.raw
for DIR in `ls $ROOT | grep D_v0_s0`
do
  pushd $ROOT/$DIR
  rm $PLACE.*.log
  rm mesh_generic.cfg
  ln -s /home/acrucker/plastisim/configs/mesh_generic.cfg mesh_generic.cfg
  sh -c "plastisim -f config.psim -p $PLACE.place -l B" 2>&1 > $PLACE.Bq13buf0rtdelay.log &
  sh -c "plastisim -f config.psim -p $PLACE.place -l B -q 4" 2>&1 > $PLACE.Bq43buf0rtdelay.log &
  sh -c "plastisim -f config.psim -p $PLACE.place -l B -q 16" 2>&1 > $PLACE.Bq163buf0rtdelay.log &
  popd
done &
for job in `jobs -p`
do
  echo $job
  wait $job || let "FAIL+=1"
done
grep -e 'DEADLOCK\|complete\|^Total DRAM' $ROOT/*/$PLACE*3buf0rtdelay.log | tee $OUT
