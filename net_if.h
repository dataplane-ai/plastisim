#ifndef NET_IF_H
#define NET_IF_H

class BooksimNetwork {
    bool can_send(int node);
    bool send(int node);
    int try_collect_token(int node);
};

#endif
