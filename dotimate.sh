#!/bin/bash

echo "Converting dot files in $1";
ls $1 | grep -e '\.dot$' | sed 's/\.dot$//' | head -n 1000 | xargs -II -P8 -n1 sh -c "echo I; dot -Tpng $1/I.dot > $1/I.png"
ffmpeg -i $1/frame%07d.png $1.mp4
