#ifndef MEMWRAP_H
#define MEMWRAP_H

#include <map>
#include <vector>
#include "DRAMSim.h"
#include "node.h"

using std::multimap;
using std::vector;
using DRAMSim::MultiChannelMemorySystem;

struct DRAMNode;

class MemWrap {
    vector<DRAMNode*> callbacks;
    multimap<uint64_t, int> pend_read;
    multimap<uint64_t, int> pend_write;
    MultiChannelMemorySystem *mem;
    string memfile;
    string sysfile;
    string rootdir;
    string tracename;
    uint64_t memsize;
    uint64_t readB, writeB;
public:
    string name;
    int id;
    MemWrap() { memsize = 16384; readB = writeB = 0; }
    int getCallbackID(DRAMNode* node);
    bool addReq(bool is_write, uint64_t addr, int id);
    void readRet(unsigned id, uint64_t addr, uint64_t cyc);
    void writeRet(unsigned id, uint64_t addr, uint64_t cyc);
    void set_param(string param, string val);
    void clock(uint64_t cycle);
    void Finalize();
    bool ready();
    void print_activity(FILE *out, uint64_t cycle);
    //~MemWrap() { delete mem; }
};

#endif
