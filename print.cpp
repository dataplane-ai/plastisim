#include <string>
#include <stdio.h>

#include "print.h"

void print_all(FILE *out, SymTab<Link>& linktab, SymTab<Node>& nodetab) {
    int count, i;
    count = linktab.count();
    fprintf(out, "Total of %d links:\n", count);
    for (i=0; i<count; i++)
        fprintf(out, "  %d: %s\n    %s\n", i, linktab.get_name(i).c_str(),
                linktab.get_ptr(i)->to_string().c_str());
    count = nodetab.count();
    fprintf(out, "Total of %d nodes:\n", count);
    for (i=0; i<count; i++)
        fprintf(out, "  %d: %s\n    %s\n", i, nodetab.get_name(i).c_str(),
                nodetab.get_ptr(i)->to_string().c_str());
}

void print_dot_node(FILE *out, Node *node) {
    int count, i;
    fprintf(out, "%s [width=3.5,label=\"%s|{{", node->name.c_str(), node->name.c_str());
    count = node->inputs.size();
    for (i=0; i<count; i++) {
        fprintf(out, "<recv__link__%s>S:%d,H:%d/%d%s", 
                node->inputs[i].link->name.c_str(),
                node->inputs[i].scale.val(),
                node->inputs[i].scaled_tokens,
                node->inputs[i].scale.val()*node->inputs[i].buffer_depth,
                i != count-1 ? "|" : "");
     /* fprintf(out, "%s%s", node->inputs[i].link->name.c_str(),
                i != count-1 ? "|" : ""); */
    }
    if (node->initial_tokens)
        fprintf(out, "S=%d", node->initial_tokens);
    fprintf(out,"}|{");
    if (node->required_tokens)
        fprintf(out, "R=%d", node->required_tokens);
    count = node->dests.size();
    for (i=0; i<count; i++) {
        fprintf(out, "<send__link__%s>S:%d,H:%d%s", 
                node->dests[i].link->name.c_str(),
                node->dests[i].scale.val(),
                node->dests[i].unscaled_tokens,
                i != count-1 ? "|" : "");
      /*fprintf(out, "%s%s", node->dests[i].link->name.c_str(),
                i != count-1 ? "|" : "");*/
    }
    fprintf(out,"}}\"];\n");
}

void print_dot_link(FILE *out, Link *link) {
    bool is_static;
    is_static = typeid(*link) != typeid(NetworkLink);
    char label[256];

    /*if (!is_static)
        snprintf(label, 256, "[taillabel=\"%d\", headlabel=\"%d\"]",
                ((NetworkLink*)link)->src_addr,
                ((NetworkLink*)link)->dst_addr);*/

    for (auto itA = link->src_node.begin(); 
            itA != link->src_node.end(); itA++) {
        for (auto itB = link->dst_node.begin(); 
                itB != link->dst_node.end(); itB++) {
            if (is_static) {
                /*snprintf(label, 256, "[label=\"L=%d\"]", 
                        ((StaticLink*)link)->lat[itB->second][itA->second]);*/
                snprintf(label, 256, "[label=\"Static\"]");
            } else {
              snprintf(label, 256, "[label=\"Net\"]");
            }
            fprintf(out, "%s:send__link__%s -> %s:recv__link__%s %s;\n",
                    itA->first->name.c_str(),
                    link->name.c_str(),
                    itB->first->name.c_str(),
                    link->name.c_str(),
                    label);
        }
    }
}

void print_dot(FILE *out, SymTab<Link>& linktab, SymTab<Node>& nodetab, uint64_t cycle) {
    int count, i;
    fprintf(out, "digraph out {\n\tnode [shape=record];\nrankdir=LR;\n");
    fprintf(out, "labelloc = \"t\"; label=\"Cycle: %lld\";\n", cycle);
    fprintf(out, "fixedsize = true;\n");
    count = nodetab.count();
    for (i=0; i<count; i++)
        print_dot_node(out, nodetab.get_ptr(i));
    count = linktab.count();
    for (i=0; i<count; i++)
        print_dot_link(out, linktab.get_ptr(i));
    fprintf(out, "}\n");
}

