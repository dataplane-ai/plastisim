#ifndef TRACEREADER_H
#define TRACEREADER_H

#include <string>
#include <fstream>

using std::string;
using std::ifstream;

class TraceReader {
    ifstream trace;
    uint64_t cur_val;
    bool is_static;
public:
    void SetFile(string file);
    void SetStatic(uint64_t val);
    void SetGeneric(string val);
    uint64_t val();
    void Advance();
};



#endif
