#ifndef OPTION_H
#define OPTION_H

class option {
    string key;
    string raw_value;
    string file;
    int lineno;

    int GetInt();
    string GetString();
    float GetFloat();
};

#endif
