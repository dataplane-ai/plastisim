#ifndef FUNC_H
#define FUNC_H

#define MAX_ARGIN   32
#define MAX_ARGOUT  32

extern float args_in[MAX_ARGIN];
extern float args_out[MAX_ARGOUT];

/* 
 * This is a pointer to a function that takes an array of void pointers, in the
 * order that they are defined in the .psim file.
 *
 * The argument is a pointer to an array of pointers to data, where the data 
 * elements are either: pointer to float/int (scalar), or pointer to array of 
 * float/int (vector). Void-typed arguments (control tokens) are not passed in
 * as part of the argument list. All arguments must be heap allocated. Scalar
 * arguments are first in the list, and vector arguments are second.
 *
 * The return type is a /sequence/ of tokens. After a function is called the 
 * first time, it should set the reference argument nagain to indicate how 
 * many times it should be called again, or to 0 if it only produces a single
 * token. The function will then be called with no input tokens nagain times
 * to process the remaining output tokens. If the function produces no tokens
 * for a given evaluation step, then it should return NULL.
 *
 * After a function is called, it takes responsibility for deallocating all
 * tokens and allocating all return tokens. The function may reuse input token
 * memory to speed up operation. 
 *
 * It is recommended that static elements in a single translation unit be used
 * to implement SRAMs and other state elements.
 *
 * */
typedef void *(*nodefunc)(int n_scal, int n_vec, void **toks, int *nagain);

#endif

