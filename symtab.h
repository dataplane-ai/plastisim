#ifndef SYMTAB_H
#define SYMTAB_H

#include <map>
#include <string>
#include <vector>
#include <assert.h>

using std::map;
using std::string;
using std::vector;

template<class T>
class SymTab {
    map<string, int> sym_tab;
    vector<string>   inv_tab;
    vector<T*>       node_ptrs;
public:
    int get_name_id(string name);
    int add_name_get_id(string name, T* val);

    T *get_ptr(int id);
    T *get_ptr(string name);

    string get_name(int id);
    int count();

    ~SymTab();
};

template<class T>
int SymTab<T>::get_name_id(string name) {
    auto s = sym_tab.find(name);
    if (s == sym_tab.end())
        //return -1;
        assert(false);
    else 
        return s->second;
}

template<class T>
int SymTab<T>::add_name_get_id(string name, T* val) {
    int next;

    // Don't readd.
    if (sym_tab.count(name))
        assert(false);
        //return -1;

    next = node_ptrs.size();
    node_ptrs.resize(next+1);
    inv_tab.resize(next+1);

    val->id = next;
    val->name = name;
    node_ptrs[next] = val;
    assert(node_ptrs[next]);

    sym_tab[name] = next;
    inv_tab[next] = name;

    return next;
}

template<class T>
T *SymTab<T>::get_ptr(int id) {
    if (id < 0)
        return NULL;
    if (id >= node_ptrs.size())
        return NULL;
    return node_ptrs[id];
}

template<class T>
T *SymTab<T>::get_ptr(string name) {
    return get_ptr(get_name_id(name));
}

template<class T>
string SymTab<T>::get_name(int id) {
    return inv_tab[id];
}

template<class T>
int SymTab<T>::count() {
    return node_ptrs.size();
}

template<class T>
SymTab<T>::~SymTab() {
    for (int i=0; i<node_ptrs.size(); i++) {
        delete node_ptrs[i];
    }
};


#endif

