#ifndef NODE_H
#define NODE_H

#include <vector>
#include <queue>
#include <list>
#include <string>
#include <stdint.h>
#include <fstream>

#include "symtab.h"
#include "DRAMSim.h"
#include "tracereader.h"
#include "memwrap.h"
//#include "link.h"
#include "intersim2/interconnect_interface.hpp"

using std::vector;
using std::deque;
using std::list;
using std::string;
using std::ifstream;

enum tok_type {k_ctrl=0, k_scal=1, k_vec=2};
enum proc_type {k_fixed=0, k_bernoulli=1, k_onoff=2};

struct Link;
struct Node;
struct MemWrap;

struct net_msg {
    int           src_node;
    int           dst_node;
    int           send_cyc;
    int           recv_cyc;
    enum tok_type type;
    int           id;
};

struct dest_spec {
    Link         *link;
    TraceReader  scale;
    int          unscaled_tokens;

    dest_spec();
    string to_string();
};

struct input_spec {
    Link         *link;
    Node         *node;
    int           count;
    TraceReader   scale;
    int           buffer_depth;
    int           scaled_tokens;
    int           unscaled_tokens;
    int           total_unscaled_tokens;
    int           pending_credits;

    bool is_space();
    bool is_enough();
    void poll_tokens(int count);

    input_spec();
    string to_string();
};

class Node {
protected:
    void                resize_node_in(int idx);
    uint64_t            active_cycles;
    uint64_t            total_outputs;
    uint64_t            stalled_cycles;
    uint64_t            starved_cycles;
    uint64_t            timeout;
    uint64_t            start_cycle;
    void                resize_node_out(int idx);
    bool                get_dram_status(uint64_t cycle);
    bool                pipestall;
    bool                pipestarve;
    void                produce_output();
    void                consume_inputs();
public:                 
    virtual void        set_param(string param, string val);
    virtual void        set_indexed_param(string param, string val, int idx);
    void                set_in_link(int idx, Link *link);
    void                set_out_link(int idx, Link *link);
    bool                trace;
    virtual void        line_trace(FILE *out);
    virtual void        line_vcd(FILE *out);
    virtual void        line_vcd_header(FILE *out);
    vector<input_spec>  inputs;
    vector<dest_spec>   dests;
    enum   proc_type    proc;
    vector<float>       proc_params;
    bool                proc_state;
    int                 expected_count;
    int                 scale_out;
    int                 initial_tokens;
    int                 required_tokens;
    int                 id;
    virtual void        Finalize();
    string              name;
    string              short_name;
                        Node();
    bool                is_satisfied();
    string              to_string();
    virtual void        clock(uint64_t cycle);
    virtual void        print_activity(FILE *out, uint64_t cycle);
    virtual bool        timed_out();
};

class DRAMNode : public Node {
    MemWrap             *mem;
    bool                is_write;
    int                 cb_idx;
    int                 tok_size;
    int                 in_tok_size;
    uint64_t            next_addr;
    int64_t             remain_burst;
    uint64_t            readB, writeB;
    int                 burst_size;
    int                 rob_entries;
    TraceReader         trace_file;
    TraceReader         size_file;
    // Tuple is <addr,returned,size>
    list<tuple<uint64_t,bool,int>> rob;
public:
    void                token_return(uint64_t addr);
    //void                line_trace(FILE *out);
                        DRAMNode();
    void                set_param(string param, string val);
    void                clock(uint64_t cycle);
    void                SetController(MemWrap *_mem);
    void                print_activity(FILE *out, uint64_t cycle);
};

class PipeNode : public Node {
protected:
    deque<int>          populated_pipeline_stages;
    int                 lat;
    void                set_lat(int lat);
public:
    //void                line_trace(FILE *out);
                        PipeNode();
    void                set_param(string param, string val);
    void                clock(uint64_t cycle);
};

class MemNode : public Node {
protected:
    int                 tile_size;
    int                 total_partitions;
    int                 writing_partition_tokens;
    int                 reading_partition_tokens;
    int                 written_partitions_to_read;
public:
    //void                line_trace(FILE *out);
                        MemNode();
    void                set_param(string param, string val);
    void                clock(uint64_t cycle);

};

#endif

